package com.nesty.pancakelibrary.initializer;

import com.google.common.base.Strings;
import com.nesty.pancakelibrary.initializer.annotation.Plugin;
import com.nesty.pancakelibrary.initializer.model.PluginConfiguration;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;
import com.nesty.pancakelibrary.initializer.model.PluginType;
import com.nesty.pancakelibrary.initializer.processor.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MetadataProcessor {

    private final AnnotationProcessor processor;
    private final List<PancakeProcessor> processors;
    private final PluginConfiguration configuration;

    public MetadataProcessor(AnnotationProcessor processor, ProcessingEnvironment environment) {
        this.processor = processor;
        this.configuration = new PluginConfiguration(environment);
        this.processors = Arrays.asList(new APIVersionProcessor(), new AuthorProcessor(), new VersionProcessor(), new WebsiteProcessor(), new DescriptionProcessor());
    }

    public boolean process(TypeElement element, PluginType type) {
        String name = element.getAnnotation(Plugin.class).name();
        Pattern NAME_PATTERN = Pattern.compile("[A-Za-z0-9-_]{0,63}");
        if (name.isEmpty()) {
            processor.error("Plugin name cannot be empty", element);
            return false;
        } else if (!NAME_PATTERN.matcher(name).matches()) {
            processor.error("Plugin name '" + name + "' must match pattern '" + NAME_PATTERN.pattern() + "'.", element);
            return false;
        }
        PluginMetadata metadata = new PluginMetadata(name, element.getQualifiedName().toString());
        processors.forEach(pancakeProcessor -> pancakeProcessor.process(metadata, element.getAnnotation(pancakeProcessor.getGenericType())));
        if (Strings.isNullOrEmpty(metadata.getVersion())) {
            processor.error("Missing @Version annotation!", element);
            return false;
        }
        try {
            configuration.write(metadata, type);
            return true;
        } catch (IOException e) {
            processor.error("An error occurred while writing configuration file!");
        }
        return false;
    }
}
