package com.nesty.pancakelibrary.initializer.parser;

import com.google.common.base.Strings;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BukkitFileParser implements PluginFileParser {

    @Override
    public Map<String, Object> parse(PluginMetadata metadata) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", metadata.getName());
        data.put("main", metadata.getMainClass());
        data.put("version", metadata.getVersion());
        if (!Strings.isNullOrEmpty(metadata.getDescription()))
            data.put("description", metadata.getDescription());
        if (!Strings.isNullOrEmpty(metadata.getAuthor()))
            data.put("author", metadata.getAuthor());
        if (!Strings.isNullOrEmpty(metadata.getWebsite()))
            data.put("website", metadata.getWebsite());
        if (!Strings.isNullOrEmpty(metadata.getApiVersion()))
            data.put("api-version", metadata.getApiVersion());
        return data;
    }

}
