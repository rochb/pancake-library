package com.nesty.pancakelibrary.initializer.processor;

import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.lang.annotation.Annotation;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PancakeProcessor {

    void process(PluginMetadata metadata, Annotation annotation);

    Class<? extends Annotation> getGenericType();
}
