package com.nesty.pancakelibrary.initializer.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface APIVersion {

    Version apiVersion();

    enum Version {
        V1_13("1.13"),
        V1_14("1.14"),
        V1_15("1.15"),
        V1_16("1.16"),
        V1_17("1.17");

        private final String raw;

        Version(String raw) {
            this.raw = raw;
        }

        public String getRaw() {
            return raw;
        }
    }
}
