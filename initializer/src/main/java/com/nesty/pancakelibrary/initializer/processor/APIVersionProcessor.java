package com.nesty.pancakelibrary.initializer.processor;

import com.nesty.pancakelibrary.initializer.annotation.APIVersion;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class APIVersionProcessor implements PancakeProcessor {

    @Override
    public void process(PluginMetadata metadata, Annotation annotation) {
        if (Objects.nonNull(annotation))
            metadata.setApiVersion(((APIVersion) annotation).apiVersion().getRaw());
    }

    @Override
    public Class<APIVersion> getGenericType() {
        return APIVersion.class;
    }

}
