package com.nesty.pancakelibrary.initializer.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Version {

    String version();

}
