package com.nesty.pancakelibrary.initializer.model;

import com.nesty.pancakelibrary.initializer.parser.BukkitFileParser;
import com.nesty.pancakelibrary.initializer.parser.BungeecordFileParser;
import com.nesty.pancakelibrary.initializer.parser.PluginFileParser;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.introspector.PropertyUtils;
import org.yaml.snakeyaml.representer.Representer;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.FileObject;
import java.io.BufferedWriter;
import java.io.IOException;

import static javax.tools.StandardLocation.CLASS_OUTPUT;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PluginConfiguration {

    private final String BUKKIT_PATH = "plugin.yml";
    private final String BUNGEECORD_PATH = "bungee.yml";
    private final ProcessingEnvironment environment;
    private static final Yaml adapter;

    static {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        options.setSplitLines(false);
        options.setIndent(2);
        options.setIndicatorIndent(0);
        Constructor yamlConstructor = new Constructor();
        PropertyUtils propertyUtils = yamlConstructor.getPropertyUtils();
        propertyUtils.setSkipMissingProperties(true);
        yamlConstructor.setPropertyUtils(propertyUtils);
        adapter = new Yaml(yamlConstructor, new Representer(), options);
    }

    public PluginConfiguration(ProcessingEnvironment environment) {
        this.environment = environment;
    }

    public void write(PluginMetadata metadata, PluginType type) throws IOException {
        String path;
        PluginFileParser parser;
        switch (type) {
            case BUKKIT -> {
                parser = new BukkitFileParser();
                path = BUKKIT_PATH;
            }
            case BUNGEECORD -> {
                parser = new BungeecordFileParser();
                path = BUNGEECORD_PATH;
            }
            case BOTH -> {
                write(metadata, PluginType.BUKKIT);
                write(metadata, PluginType.BUNGEECORD);
                return;
            }
            default -> throw new IllegalStateException("Unexpected value: " + type);
        }
        FileObject object = environment.getFiler()
                .createResource(CLASS_OUTPUT, "", path);
        BufferedWriter writer = new BufferedWriter(object.openWriter());
        writer.append("#############################################\n")
                .append("#            Pancake Initializer            #\n")
                .append("#                                           #\n")
                .append("# Auto generated file                       #\n")
                .append("# www.roch-blondiaux.com                    #\n")
                .append("#############################################\n\n");
        adapter.dump(parser.parse(metadata), writer);
    }
}
