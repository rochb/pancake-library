package com.nesty.pancakelibrary.initializer.parser;

import com.google.common.base.Strings;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class BungeecordFileParser implements PluginFileParser {

    @Override
    public Map<String, Object> parse(PluginMetadata metadata) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", metadata.getName());
        data.put("main", metadata.getMainClass());
        data.put("version", metadata.getVersion());
        if (!Strings.isNullOrEmpty(metadata.getDescription()))
            data.put("description", metadata.getDescription());
        if (!Strings.isNullOrEmpty(metadata.getAuthor()))
            data.put("author", metadata.getAuthor());
        return data;
    }

}
