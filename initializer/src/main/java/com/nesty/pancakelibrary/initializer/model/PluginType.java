package com.nesty.pancakelibrary.initializer.model;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum PluginType {
    BUKKIT("org.bukkit.plugin.java.JavaPlugin"),
    BUNGEECORD("net.md_5.bungee.api.plugin.Plugin"),
    BOTH(null),
    UNKNOWN(null);

    private final String name;

    PluginType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
