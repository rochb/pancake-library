package com.nesty.pancakelibrary.initializer;

import com.nesty.pancakelibrary.initializer.annotation.Plugin;
import com.nesty.pancakelibrary.initializer.model.PluginType;
import com.nesty.pancakelibrary.initializer.utils.ReflectionUtils;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@SupportedAnnotationTypes({
        "com.nesty.pancakelibrary.initializer.annotation.Plugin",
        "com.nesty.pancakelibrary.initializer.annotation.Author",
        "com.nesty.pancakelibrary.initializer.annotation.Description",
        "com.nesty.pancakelibrary.initializer.annotation.Version",
        "com.nesty.pancakelibrary.initializer.annotation.Website"
})
@SupportedSourceVersion(SourceVersion.RELEASE_16)
public class AnnotationProcessor extends AbstractProcessor {

    private PluginType type;
    private MetadataProcessor metadataProcessor;

    @Override
    public synchronized void init(ProcessingEnvironment environment) {
        super.init(environment);
        this.metadataProcessor = new MetadataProcessor(this, environment);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (roundEnv.processingOver() || !ReflectionUtils.contains(annotations, Plugin.class))
            return false;

        info("Processing @Plugin annotation...");
        type = ReflectionUtils.getPluginType(processingEnv);
        if (type.equals(PluginType.UNKNOWN)) {
            error("Couldn't find org.bukkit.plugin.java.JavaPlugin nor net.md_5.bungee.api.plugin.Plugin in classpath.");
            return false;
        }

        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(Plugin.class);
        filterElements(elements);

        if (elements.size() == 0)
            return false;
        else if (elements.size() > 2) {
            List<String> classes = new ArrayList<>();
            elements.forEach(element -> classes.add(element.getSimpleName().toString()));
            error("More than two classes annotated with @Plugin found: " + Arrays.toString(classes.toArray(new String[]{})));
            return false;
        } else if (type.equals(PluginType.BOTH)) {
            Iterator<? extends Element> it = elements.iterator();
            TypeElement first = (TypeElement) it.next();
            TypeElement second = (TypeElement) it.next();
            TypeMirror bukkitType = ReflectionUtils.getTypeByClass(processingEnv, "org.bukkit.plugin.java.JavaPlugin");
            TypeMirror bungeeType = ReflectionUtils.getTypeByClass(processingEnv, "net.md_5.bungee.api.plugin.Plugin");

            if (processingEnv.getTypeUtils().isSubtype(first.asType(), bukkitType)
                    && processingEnv.getTypeUtils().isSubtype(second.asType(), bukkitType))
                error("Both elements annotated with @Plugin extends org.bukkit.plugin.java.JavaPlugin");
            if (processingEnv.getTypeUtils().isSubtype(first.asType(), bukkitType)) {
                metadataProcessor.process(first, PluginType.BUKKIT);
                metadataProcessor.process(second, PluginType.BUNGEECORD);
                return true;
            }
            if (processingEnv.getTypeUtils().isSubtype(first.asType(), bungeeType)
                    && processingEnv.getTypeUtils().isSubtype(second.asType(), bungeeType)) {
                error("Both elements annotated with @Plugin extends net.md_5.bungee.api.plugin.Plugin");
            }
            if (processingEnv.getTypeUtils().isSubtype(first.asType(), bungeeType)) {
                metadataProcessor.process(second, PluginType.BUKKIT);
                metadataProcessor.process(first, PluginType.BUNGEECORD);
                return true;
            }
        }
        return metadataProcessor.process((TypeElement) elements.iterator().next(), type);
    }

    private void filterElements(Set<? extends Element> elements) {
        elements.removeIf(element -> {
            if (element.getKind() != ElementKind.CLASS) {
                warn("Invalid element of type " + element.getKind() + " annotated with @Plugin");
                return true;
            }
            if (!(element.getEnclosingElement() instanceof PackageElement) && !element.getModifiers().contains(Modifier.STATIC)) {
                error("Element annotated with @Plugin is not top-level or static nested", element);
                return true;
            }
            if (type.equals(PluginType.BOTH)) {
                if (ReflectionUtils.isNotSubType(processingEnv, element, PluginType.BUKKIT)
                        && ReflectionUtils.isNotSubType(processingEnv, element, PluginType.BUNGEECORD)) {
                    error("Element annotated with @Plugin doesn't extends org.bukkit.plugin.java.JavaPlugin nor net.md_5.bungee.api.plugin.Plugin", element);
                    return true;
                }
            } else if (type.equals(PluginType.BUKKIT)) {
                if (ReflectionUtils.isNotSubType(processingEnv, element, PluginType.BUKKIT)) {
                    error("Element annotated with @Plugin doesn't extends org.bukkit.plugin.java.JavaPlugin", element);
                    return true;
                }
            } else {
                if (ReflectionUtils.isNotSubType(processingEnv, element, PluginType.BUNGEECORD)) {
                    error("Element annotated with @Plugin doesn't extends net.md_5.bungee.api.plugin.Plugin", element);
                    return true;
                }
            }
            return false;
        });
    }

    /* Logging */
    public void info(String message) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, message);
    }

    public void warn(String message) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, message);
    }

    public void error(String message) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message);
    }

    public void error(String message, Element element) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message, element);
    }
}
