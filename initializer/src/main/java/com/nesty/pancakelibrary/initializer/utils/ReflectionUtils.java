package com.nesty.pancakelibrary.initializer.utils;

import com.nesty.pancakelibrary.initializer.model.PluginType;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ReflectionUtils {

    /**
     * Tests if the given collection of elements contains the given annotation
     * class.
     *
     * @param elements The elements to search in
     * @param clazz    The annotation class to look for
     * @return true if found false otherwise
     */
    public static boolean contains(Collection<? extends TypeElement> elements, Class<?> clazz) {
        if (elements.isEmpty())
            return false;
        return elements.stream().anyMatch(element -> element.getQualifiedName().contentEquals(clazz.getName()));
    }

    public static boolean isChildOf(TypeElement element, ProcessingEnvironment environment, String className) {
        while (element != null && element.getSuperclass().getKind() != TypeKind.NONE) {
            element = environment.getElementUtils().getTypeElement(element.getSuperclass().toString());
            if (element.getQualifiedName().contentEquals(className))
                return true;
        }
        return false;
    }

    /**
     * Gets an {@link TypeMirror} for the given class name.
     *
     * @param className The class name
     * @return The {@link TypeMirror} if it is found else null.
     */
    public static TypeMirror getTypeByClass(ProcessingEnvironment environment, String className) {
        TypeElement element = environment.getElementUtils().getTypeElement(className);
        if (Objects.isNull(element))
            return null;
        return element.asType();
    }

    public static boolean isNotSubType(ProcessingEnvironment environment, Element element, PluginType type) {
        if (!type.equals(PluginType.BUNGEECORD) && !type.equals(PluginType.BUKKIT))
            return true;
        return !environment.getTypeUtils().isSubtype(element.asType(), getTypeByClass(environment, type.getName()));
    }

    public static PluginType getPluginType(ProcessingEnvironment environment) {
        TypeMirror spigotType = getTypeByClass(environment, "org.bukkit.plugin.java.JavaPlugin");
        TypeMirror bungeeType = getTypeByClass(environment, "net.md_5.bungee.api.plugin.Plugin");
        if (Objects.isNull(spigotType)
                && Objects.isNull(bungeeType))
            return PluginType.UNKNOWN;
        if (Objects.isNull(spigotType))
            return PluginType.BUNGEECORD;
        else if (Objects.isNull(bungeeType))
            return PluginType.BUKKIT;
        return PluginType.BOTH;
    }
}
