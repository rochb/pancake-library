package com.nesty.pancakelibrary.initializer.parser;

import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PluginFileParser {

    Map<String, Object> parse(PluginMetadata metadata);

}
