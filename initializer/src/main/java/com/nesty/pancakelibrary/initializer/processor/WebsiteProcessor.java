package com.nesty.pancakelibrary.initializer.processor;

import com.nesty.pancakelibrary.initializer.annotation.Website;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class WebsiteProcessor implements PancakeProcessor {

    @Override
    public void process(PluginMetadata metadata, Annotation annotation) {
        if (Objects.nonNull(annotation))
            metadata.setWebsite(((Website) annotation).website());
    }

    @Override
    public Class<Website> getGenericType() {
        return Website.class;
    }

}
