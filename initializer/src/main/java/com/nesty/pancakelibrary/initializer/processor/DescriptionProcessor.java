package com.nesty.pancakelibrary.initializer.processor;

import com.nesty.pancakelibrary.initializer.annotation.Description;
import com.nesty.pancakelibrary.initializer.annotation.Version;
import com.nesty.pancakelibrary.initializer.model.PluginMetadata;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DescriptionProcessor implements PancakeProcessor {

    @Override
    public void process(PluginMetadata metadata, Annotation annotation) {
        if (Objects.nonNull(annotation))
            metadata.setDescription(((Description) annotation).description());
    }

    @Override
    public Class<Description> getGenericType() {
        return Description.class;
    }

}
