package com.nesty.pancakelibrary.initializer.model;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PluginMetadata {

    private final String name;
    private final String mainClass;
    private String description;
    private String version;
    private String author;
    private String website;
    private String apiVersion;

    public PluginMetadata(String name, String mainClass) {
        this.name = name;
        this.mainClass = mainClass;
    }

    public String getName() {
        return name;
    }

    public String getMainClass() {
        return mainClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
