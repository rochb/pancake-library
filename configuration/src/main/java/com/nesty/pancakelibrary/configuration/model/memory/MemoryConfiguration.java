package com.nesty.pancakelibrary.configuration.model.memory;

import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * This is a {@link Configuration} implementation that does not save or load
 * from any source, and stores all values in memory only.
 * This is useful for temporary Configurations for providing defaults.
 */
public class MemoryConfiguration extends MemorySection implements Configuration {

    protected MemoryConfigurationOptions options;

    /**
     * Creates an empty {@link MemoryConfiguration} with no default values.
     */
    public MemoryConfiguration() {
    }

    @Override
    public ConfigurationSection getParent() {
        return null;
    }

    @Override
    @NonNull
    public MemoryConfigurationOptions options() {
        if (options == null)
            options = new MemoryConfigurationOptions(this);
        return options;
    }
}