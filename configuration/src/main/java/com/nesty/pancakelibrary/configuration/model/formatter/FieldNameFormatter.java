package com.nesty.pancakelibrary.configuration.model.formatter;

import java.util.function.Function;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@FunctionalInterface
public interface FieldNameFormatter extends Function<String, String> {

    String fromFieldName(String fieldName);

    @Override
    default String apply(String s) {
        return fromFieldName(s);
    }
}