package com.nesty.pancakelibrary.configuration.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class InvalidValueException extends IllegalArgumentException {

    public InvalidValueException(String s) {
        super(s);
    }
}
