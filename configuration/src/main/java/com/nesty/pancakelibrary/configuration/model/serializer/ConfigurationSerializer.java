package com.nesty.pancakelibrary.configuration.model.serializer;

import com.nesty.pancakelibrary.configuration.exceptions.InvalidValueException;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface ConfigurationSerializer<T> {

    Optional<T> deserialize(@NonNull Configuration configuration, @NonNull String key) throws InvalidValueException;

    void serialize(@NonNull Configuration configuration, @NonNull String key, @NonNull Object object) throws InvalidValueException;

    default List<T> deserializeList(@NonNull Configuration configuration, @NonNull String key) throws InvalidValueException {
        if (!configuration.isSet(key) || !configuration.isConfigurationSection(key))
            return new ArrayList<>();
        ConfigurationSection section = configuration.getConfigurationSection(key);
        return section.getKeys(true)
                .stream()
                .map(section::getConfigurationSection)
                .filter(Objects::nonNull)
                .map(section1 -> deserialize(configuration, section1.getCurrentPath()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
