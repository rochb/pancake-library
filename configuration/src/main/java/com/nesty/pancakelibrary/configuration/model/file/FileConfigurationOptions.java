package com.nesty.pancakelibrary.configuration.model.file;

import com.nesty.pancakelibrary.configuration.model.memory.MemoryConfiguration;
import com.nesty.pancakelibrary.configuration.model.memory.MemoryConfigurationOptions;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * Various settings for controlling the input and output of a {@link
 * FileConfiguration}
 */
public class FileConfigurationOptions extends MemoryConfigurationOptions {

    protected FileConfigurationOptions(@NonNull MemoryConfiguration configuration) {
        super(configuration);
    }

    @NonNull
    public FileConfiguration getConfiguration() {
        return (FileConfiguration) super.getConfiguration();
    }
}