package com.nesty.pancakelibrary.configuration.model;

import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * Represents a source of configurable options and settings
 */
public interface Configuration extends ConfigurationSection {

    /**
     * Gets the {@link ConfigurationOptions} for this {@link Configuration}.
     * <p>
     * All setters through this method are chainable.
     *
     * @return Options for this configuration
     */
    @NonNull ConfigurationOptions options();
}