package com.nesty.pancakelibrary.configuration.repository;

import com.nesty.pancakelibrary.commons.utils.ReflectionUtils;
import com.nesty.pancakelibrary.configuration.model.serializer.ConfigurationSerializer;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class SerializerRepository {

    private final List<ConfigurationSerializer<?>> serializers = new ArrayList<>();

    public void add(@NonNull ConfigurationSerializer<?> serializer) {
        this.serializers.add(serializer);
    }

    public void remove(@NonNull ConfigurationSerializer<?> serializer) {
        this.serializers.remove(serializer);
    }

    public Optional<ConfigurationSerializer<?>> getByTarget(@NonNull Class<?> clazz) {
        return serializers.stream()
                .filter(serializer -> ReflectionUtils.getGenericType(serializer.getClass())
                        .filter(aClass -> aClass.equals(clazz))
                        .isPresent())
                .findFirst();
    }

    public Optional<ConfigurationSerializer<?>> getByClass(@NonNull Class<?> clazz) {
        return serializers.stream()
                .filter(serializer -> serializer.getClass().equals(clazz))
                .findFirst();
    }
}
