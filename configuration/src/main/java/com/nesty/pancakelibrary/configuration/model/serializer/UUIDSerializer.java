package com.nesty.pancakelibrary.configuration.model.serializer;

import com.nesty.pancakelibrary.configuration.exceptions.InvalidValueException;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import lombok.NonNull;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UUIDSerializer implements ConfigurationSerializer<UUID> {

    @Override
    public Optional<UUID> deserialize(@NonNull Configuration configuration, @NonNull String key) throws InvalidValueException {
        if (!configuration.isSet(key)
                || !configuration.isString(key))
            return Optional.empty();
        try {
            UUID uuid = UUID.fromString(configuration.getString(key));
            return Optional.of(uuid);
        } catch (Exception ignored) {
            throw new InvalidValueException(String.format("Invalid value for %s!", key));
        }
    }

    @Override
    public void serialize(@NonNull Configuration configuration, @NonNull String key, @NonNull Object object) throws InvalidValueException {
        configuration.set(key, object.toString());
    }

}
