package com.nesty.pancakelibrary.configuration.model.memory;

import com.nesty.pancakelibrary.commons.utils.Validate;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import lombok.NonNull;

import java.util.*;

import static com.nesty.pancakelibrary.commons.NumberUtils.*;

/**
 * A type of {@link ConfigurationSection} that is stored in memory.
 */
public class MemorySection implements ConfigurationSection {

    protected final Map<String, Object> map = new LinkedHashMap<>();
    private final Configuration root;
    private final ConfigurationSection parent;
    private final String path;
    private final String fullPath;

    /**
     * Creates an empty MemorySection for use as a root {@link Configuration}
     * section.
     * <p>
     * Note that calling this without being yourself a {@link Configuration}
     * will throw an exception!
     *
     * @throws IllegalStateException Thrown if this is not a {@link
     *                               Configuration} root.
     */
    protected MemorySection() {
        if (!(this instanceof Configuration))
            throw new IllegalStateException("Cannot construct a root MemorySection when not a Configuration");
        this.path = "";
        this.fullPath = "";
        this.parent = null;
        this.root = (Configuration) this;
    }

    /**
     * Creates an empty MemorySection with the specified parent and path.
     *
     * @param parent Parent section that contains this own section.
     * @param path   Path that you may access this section from via the root
     *               {@link Configuration}.
     * @throws IllegalArgumentException Thrown is parent or path is null, or
     *                                  if parent contains no root Configuration.
     */
    protected MemorySection(@NonNull ConfigurationSection parent, @NonNull String path) {
        Validate.notNull(parent, "Parent cannot be null");
        Validate.notNull(path, "Path cannot be null");

        this.path = path;
        this.parent = parent;
        this.root = parent.getRoot();

        Validate.notNull(root, "Path cannot be orphaned");

        this.fullPath = createPath(parent, path);
    }

    @Override
    @NonNull
    public Set<String> getKeys(boolean deep) {
        Set<String> result = new LinkedHashSet<>();
        mapChildrenKeys(result, this, deep);
        return result;
    }

    @Override
    @NonNull
    public Map<String, Object> getValues(boolean deep) {
        Map<String, Object> result = new LinkedHashMap<>();
        mapChildrenValues(result, this, deep);
        return result;
    }

    @Override
    public boolean contains(@NonNull String path) {
        return contains(path, false);
    }

    @Override
    public boolean contains(@NonNull String path, boolean ignoreDefault) {
        return ((ignoreDefault) ? get(path, null) : get(path)) != null;
    }

    @Override
    public boolean isSet(@NonNull String path) {
        Configuration root = getRoot();
        if (root == null)
            return false;
        return get(path, null) != null;
    }

    @Override
    @NonNull
    public String getCurrentPath() {
        return fullPath;
    }

    @Override
    @NonNull
    public String getName() {
        return path;
    }

    @Override
    public Configuration getRoot() {
        return root;
    }

    @Override
    public ConfigurationSection getParent() {
        return parent;
    }

    @Override
    public void set(@NonNull String path, Object value) {
        Validate.notEmpty(path, "Cannot set to an empty path");

        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot use section without a root");

        final char separator = root.options().getPathSeparator();
        // i1 is the leading (higher) index
        // i2 is the trailing (lower) index
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            String node = path.substring(i2, i1);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null) {
                if (value == null)
                    // no need to create missing sub-sections if we want to remove the value:
                    return;
                section = section.createSection(node);
            } else
                section = subSection;
        }

        String key = path.substring(i2);
        if (section == this) {
            if (value == null)
                map.remove(key);
            else
                map.put(key, value);
        } else
            section.set(key, value);
    }

    @Override
    public Object get(@NonNull String path) {
        return get(path, null);
    }

    @Override
    public Object get(@NonNull String path, Object def) {
        Validate.notNull(path, "Path cannot be null");

        if (path.length() == 0)
            return this;

        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot access section without a root");

        final char separator = root.options().getPathSeparator();
        // i1 is the leading (higher) index
        // i2 is the trailing (lower) index
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            section = section.getConfigurationSection(path.substring(i2, i1));
            if (section == null)
                return def;
        }

        String key = path.substring(i2);
        if (section == this) {
            Object result = map.get(key);
            return (result == null) ? def : result;
        }
        return section.get(key, def);
    }

    @Override
    @NonNull
    public ConfigurationSection createSection(@NonNull String path) {
        Validate.notEmpty(path, "Cannot create section at empty path");
        Configuration root = getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot create section without a root");

        final char separator = root.options().getPathSeparator();
        // i1 is the leading (higher) index
        // i2 is the trailing (lower) index
        int i1 = -1, i2;
        ConfigurationSection section = this;
        while ((i1 = path.indexOf(separator, i2 = i1 + 1)) != -1) {
            String node = path.substring(i2, i1);
            ConfigurationSection subSection = section.getConfigurationSection(node);
            if (subSection == null)
                section = section.createSection(node);
            else
                section = subSection;
        }

        String key = path.substring(i2);
        if (section == this) {
            ConfigurationSection result = new MemorySection(this, key);
            map.put(key, result);
            return result;
        }
        return section.createSection(key);
    }

    @Override
    @NonNull
    public ConfigurationSection createSection(@NonNull String path, @NonNull Map<?, ?> map) {
        ConfigurationSection section = createSection(path);

        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (entry.getValue() instanceof Map)
                section.createSection(entry.getKey().toString(), (Map<?, ?>) entry.getValue());
            else
                section.set(entry.getKey().toString(), entry.getValue());
        }

        return section;
    }

    // Primitives
    @Override
    public String getString(@NonNull String path) {
        return getString(path, null);
    }

    @Override
    public String getString(@NonNull String path, String def) {
        Object val = get(path, def);
        return (val != null) ? val.toString() : def;
    }

    @Override
    public boolean isString(@NonNull String path) {
        Object val = get(path);
        return val instanceof String;
    }

    @Override
    public int getInt(@NonNull String path) {
        return getInt(path, 0);
    }

    @Override
    public int getInt(@NonNull String path, int def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toInt(val) : def;
    }

    @Override
    public boolean isInt(@NonNull String path) {
        Object val = get(path);
        return val instanceof Integer;
    }

    @Override
    public boolean getBoolean(@NonNull String path) {
        return getBoolean(path, false);
    }

    @Override
    public boolean getBoolean(@NonNull String path, boolean def) {
        Object val = get(path, def);
        return (val instanceof Boolean) ? (Boolean) val : def;
    }

    @Override
    public boolean isBoolean(@NonNull String path) {
        Object val = get(path);
        return val instanceof Boolean;
    }

    @Override
    public double getDouble(@NonNull String path) {
        return getDouble(path, 0);
    }

    @Override
    public double getDouble(@NonNull String path, double def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toDouble(val) : def;
    }

    @Override
    public boolean isDouble(@NonNull String path) {
        Object val = get(path);
        return val instanceof Double;
    }

    @Override
    public long getLong(@NonNull String path) {
        return getLong(path, 0);
    }

    @Override
    public long getLong(@NonNull String path, long def) {
        Object val = get(path, def);
        return (val instanceof Number) ? toLong(val) : def;
    }

    @Override
    public boolean isLong(@NonNull String path) {
        Object val = get(path);
        return val instanceof Long;
    }

    // Java
    @Override
    public List<?> getList(@NonNull String path) {
        return getList(path, null);
    }

    @Override
    public List<?> getList(@NonNull String path, List<?> def) {
        Object val = get(path, def);
        return (List<?>) ((val instanceof List) ? val : def);
    }

    @Override
    public boolean isList(@NonNull String path) {
        Object val = get(path);
        return val instanceof List;
    }

    @Override
    @NonNull
    public List<String> getStringList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        List<String> result = new ArrayList<>();

        for (Object object : list) {
            if ((object instanceof String) || (isPrimitiveWrapper(object))) {
                result.add(String.valueOf(object));
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Integer> getIntegerList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        List<Integer> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Integer)
                result.add((Integer) object);
            else if (object instanceof String) {
                try {
                    result.add(Integer.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character)
                result.add((int) (Character) object);
            else if (object instanceof Number)
                result.add(((Number) object).intValue());
        }

        return result;
    }

    @Override
    @NonNull
    public List<Boolean> getBooleanList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        List<Boolean> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Boolean) {
                result.add((Boolean) object);
            } else if (object instanceof String) {
                if (Boolean.TRUE.toString().equals(object))
                    result.add(true);
                else if (Boolean.FALSE.toString().equals(object))
                    result.add(false);
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Double> getDoubleList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null)
            return new ArrayList<>(0);

        List<Double> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Double) {
                result.add((Double) object);
            } else if (object instanceof String) {
                try {
                    result.add(Double.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character) {
                result.add((double) (Character) object);
            } else if (object instanceof Number) {
                result.add(((Number) object).doubleValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Float> getFloatList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null) {
            return new ArrayList<>(0);
        }

        List<Float> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Float) {
                result.add((Float) object);
            } else if (object instanceof String) {
                try {
                    result.add(Float.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character) {
                result.add((float) (Character) object);
            } else if (object instanceof Number) {
                result.add(((Number) object).floatValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Long> getLongList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null) {
            return new ArrayList<>(0);
        }

        List<Long> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Long) {
                result.add((Long) object);
            } else if (object instanceof String) {
                try {
                    result.add(Long.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character) {
                result.add((long) (Character) object);
            } else if (object instanceof Number) {
                result.add(((Number) object).longValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Byte> getByteList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null) {
            return new ArrayList<>(0);
        }

        List<Byte> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Byte) {
                result.add((Byte) object);
            } else if (object instanceof String) {
                try {
                    result.add(Byte.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character) {
                result.add((byte) ((Character) object).charValue());
            } else if (object instanceof Number) {
                result.add(((Number) object).byteValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Character> getCharacterList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null) {
            return new ArrayList<>(0);
        }

        List<Character> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Character) {
                result.add((Character) object);
            } else if (object instanceof String) {
                String str = (String) object;

                if (str.length() == 1) {
                    result.add(str.charAt(0));
                }
            } else if (object instanceof Number) {
                result.add((char) ((Number) object).intValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Short> getShortList(@NonNull String path) {
        List<?> list = getList(path);

        if (list == null) {
            return new ArrayList<>(0);
        }

        List<Short> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Short) {
                result.add((Short) object);
            } else if (object instanceof String) {
                try {
                    result.add(Short.valueOf((String) object));
                } catch (Exception ignored) {
                }
            } else if (object instanceof Character) {
                result.add((short) ((Character) object).charValue());
            } else if (object instanceof Number) {
                result.add(((Number) object).shortValue());
            }
        }

        return result;
    }

    @Override
    @NonNull
    public List<Map<?, ?>> getMapList(@NonNull String path) {
        List<?> list = getList(path);
        List<Map<?, ?>> result = new ArrayList<>();

        if (list == null) {
            return result;
        }

        for (Object object : list) {
            if (object instanceof Map) {
                result.add((Map<?, ?>) object);
            }
        }

        return result;
    }

    // Bukkit
    @Override
    public <T> T getObject(@NonNull String path, @NonNull Class<T> clazz) {
        Validate.notNull(clazz, "Class cannot be null");
        return getObject(path, clazz, null);
    }

    @Override
    public <T> T getObject(@NonNull String path, @NonNull Class<T> clazz, T def) {
        Validate.notNull(clazz, "Class cannot be null");
        Object val = get(path, def);
        return (clazz.isInstance(val)) ? clazz.cast(val) : def;
    }

    @Override
    public ConfigurationSection getConfigurationSection(@NonNull String path) {
        Object val = get(path, null);
        if (val != null)
            return (val instanceof ConfigurationSection) ? (ConfigurationSection) val : null;
        return createSection(path);
    }

    @Override
    public boolean isConfigurationSection(@NonNull String path) {
        Object val = get(path);
        return val instanceof ConfigurationSection;
    }

    protected boolean isPrimitiveWrapper(Object input) {
        return input instanceof Integer || input instanceof Boolean
                || input instanceof Character || input instanceof Byte
                || input instanceof Short || input instanceof Double
                || input instanceof Long || input instanceof Float;
    }

    protected void mapChildrenKeys(@NonNull Set<String> output, @NonNull ConfigurationSection section, boolean deep) {
        if (section instanceof MemorySection) {
            MemorySection sec = (MemorySection) section;

            for (Map.Entry<String, Object> entry : sec.map.entrySet()) {
                output.add(createPath(section, entry.getKey(), this));

                if ((deep) && (entry.getValue() instanceof ConfigurationSection)) {
                    ConfigurationSection subsection = (ConfigurationSection) entry.getValue();
                    mapChildrenKeys(output, subsection, deep);
                }
            }
        } else {
            Set<String> keys = section.getKeys(deep);

            for (String key : keys) {
                output.add(createPath(section, key, this));
            }
        }
    }

    protected void mapChildrenValues(@NonNull Map<String, Object> output, @NonNull ConfigurationSection section, boolean deep) {
        if (section instanceof MemorySection) {
            MemorySection sec = (MemorySection) section;

            for (Map.Entry<String, Object> entry : sec.map.entrySet()) {
                // Because of the copyDefaults call potentially copying out of order, we must remove and then add in our saved order
                // This means that default values we haven't set end up getting placed first
                // See SPIGOT-4558 for an example using spigot.yml - watch subsections move around to default order
                String childPath = createPath(section, entry.getKey(), this);
                output.remove(childPath);
                output.put(childPath, entry.getValue());

                if (entry.getValue() instanceof ConfigurationSection) {
                    if (deep)
                        mapChildrenValues(output, (ConfigurationSection) entry.getValue(), deep);
                }
            }
        } else {
            Map<String, Object> values = section.getValues(deep);

            for (Map.Entry<String, Object> entry : values.entrySet())
                output.put(createPath(section, entry.getKey(), this), entry.getValue());
        }
    }

    /**
     * Creates a full path to the given {@link ConfigurationSection} from its
     * root {@link Configuration}.
     * <p>
     * You may use this method for any given {@link ConfigurationSection}, not
     * only {@link MemorySection}.
     *
     * @param section Section to create a path for.
     * @param key     Name of the specified section.
     * @return Full path of the section from its root.
     */
    @NonNull
    public static String createPath(@NonNull ConfigurationSection section, String key) {
        return createPath(section, key, section.getRoot());
    }

    /**
     * Creates a relative path to the given {@link ConfigurationSection} from
     * the given relative section.
     * <p>
     * You may use this method for any given {@link ConfigurationSection}, not
     * only {@link MemorySection}.
     *
     * @param section    Section to create a path for.
     * @param key        Name of the specified section.
     * @param relativeTo Section to create the path relative to.
     * @return Full path of the section from its root.
     */
    @NonNull
    public static String createPath(@NonNull ConfigurationSection section, String key, ConfigurationSection relativeTo) {
        Validate.notNull(section, "Cannot create path without a section");
        Configuration root = section.getRoot();
        if (root == null)
            throw new IllegalStateException("Cannot create path without a root");
        char separator = root.options().getPathSeparator();

        StringBuilder builder = new StringBuilder();
        for (ConfigurationSection parent = section; (parent != null) && (parent != relativeTo); parent = parent.getParent()) {
            if (builder.length() > 0)
                builder.insert(0, separator);

            builder.insert(0, parent.getName());
        }

        if ((key != null) && (key.length() > 0)) {
            if (builder.length() > 0)
                builder.append(separator);

            builder.append(key);
        }

        return builder.toString();
    }

    @Override
    public String toString() {
        Configuration root = getRoot();
        return getClass().getSimpleName() +
                "[path='" +
                getCurrentPath() +
                "', root='" +
                (root == null ? null : root.getClass().getSimpleName()) +
                "']";
    }
}
