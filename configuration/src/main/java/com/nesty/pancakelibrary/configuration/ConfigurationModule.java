package com.nesty.pancakelibrary.configuration;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.configuration.exceptions.InvalidConfigurationException;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.file.FileConfiguration;
import com.nesty.pancakelibrary.configuration.model.serializer.UUIDSerializer;
import com.nesty.pancakelibrary.configuration.model.wrapper.YamlConfigurationWrapper;
import com.nesty.pancakelibrary.configuration.service.SerializerService;
import com.nesty.pancakelibrary.modules.CommonModules;
import com.nesty.pancakelibrary.modules.model.module.JavaModule;
import lombok.Getter;
import lombok.NonNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class ConfigurationModule extends JavaModule {

    private final SerializerService serializerService;
    private final List<Configuration> configurations = new ArrayList<>();

    public ConfigurationModule() {
        super(CommonModules.CONFIGURATION.getName());
        this.serializerService = new SerializerService();
    }

    @Override
    public void load(@NonNull PancakePlugin plugin) {
        /* Integrated Serializers */
        this.serializerService.add(UUIDSerializer.class);
    }

    @Override
    public void unload() {
        this.serializerService.getSerializers().clear();
    }

    public <T extends Configuration> T register(@NonNull T configuration) {
        configurations.add(configuration);
        if (configuration instanceof FileConfiguration) {
            try {
                if (configuration instanceof YamlConfigurationWrapper)
                    ((YamlConfigurationWrapper) configuration).setSerializerService(serializerService);
                ((FileConfiguration) configuration).load();
            } catch (IOException | InvalidConfigurationException e) {
                PancakeLogger.get().error("Cannot load %s configuration file!", ((FileConfiguration) configuration).getFile().getName());
                e.printStackTrace();
            }
        }
        return configuration;
    }

    public void reload() {
        configurations
                .stream()
                .filter(configuration -> configuration instanceof FileConfiguration)
                .map(configuration -> (FileConfiguration) configuration)
                .forEach(configuration -> {
                    try {
                        if (configuration instanceof YamlConfigurationWrapper)
                            ((YamlConfigurationWrapper) configuration).setSerializerService(serializerService);
                        configuration.load();
                    } catch (IOException | InvalidConfigurationException e) {
                        PancakeLogger.get().error("Cannot load %s configuration file!", configuration.getFile().getName());
                        e.printStackTrace();
                    }
                });
    }

    public boolean isRegistered(@NonNull Class<? extends Configuration> clazz) {
        return configurations.stream()
                .anyMatch(configuration -> configuration.getClass().equals(clazz));
    }

    public Optional<Configuration> getByClass(@NonNull Class<? extends Configuration> clazz) {
        return configurations.stream()
                .filter(configuration -> configuration.getClass().equals(clazz))
                .findFirst();
    }

    @Override
    public List<PancakeDependency> getDependencies() {
        return Collections.singletonList(PancakeDependency.SNAKEYAML);
    }
}
