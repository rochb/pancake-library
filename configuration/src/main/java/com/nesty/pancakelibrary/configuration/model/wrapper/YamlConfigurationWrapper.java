package com.nesty.pancakelibrary.configuration.model.wrapper;

import com.nesty.pancakelibrary.commons.utils.StringUtils;
import com.nesty.pancakelibrary.configuration.exceptions.InvalidConfigurationException;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import com.nesty.pancakelibrary.configuration.model.annotation.Configurable;
import com.nesty.pancakelibrary.configuration.model.formatter.FieldNameFormatters;
import com.nesty.pancakelibrary.configuration.model.serializer.ConfigurationSerializer;
import com.nesty.pancakelibrary.configuration.model.yaml.YamlConfiguration;
import com.nesty.pancakelibrary.configuration.service.SerializerService;
import lombok.NonNull;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class YamlConfigurationWrapper extends YamlConfiguration {

    @Setter
    private SerializerService serializerService;

    public YamlConfigurationWrapper(@NonNull File file) {
        super(file);
    }

    @Override
    public void save() throws IOException {
        getAnnotatedFields().forEach((key, field) -> {
            Object value = null;
            try {
                field.setAccessible(true);
                value = field.get(this);
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            Optional<ConfigurationSerializer<?>> v = serializerService.getByTarget(field.getType());
            if (Objects.isNull(value))
                return;
            if (v.isPresent())
                v.get().serialize(this, key, value);
            else
                set(key, value);
        });
        super.save();
    }

    @Override
    public void load() throws IOException, InvalidConfigurationException {
        super.load();

        getAnnotatedFields().forEach((key, field) -> {
            Object value = null;
            /* Configuration Section */
            if (isConfigurationSection(key)) {
                ConfigurationSection section = getConfigurationSection(key);
                if (field.getType().equals(ConfigurationSection.class))
                    value = section;
                else if (field.getGenericType() instanceof ParameterizedType
                        && field.getType().equals(List.class)) {
                    ParameterizedType genericType = (ParameterizedType) field.getGenericType();
                    Class<?> fieldType = (Class<?>) genericType.getActualTypeArguments()[0];
                    value = serializerService.deserializeList(this, section, fieldType);
                    if (Objects.isNull(value)) value = new ArrayList<>();
                }
            }
            if (Objects.isNull(value))
                value = serializerService
                        .deserialize(this, key, field.getType())
                        .orElse(null);
            if (Objects.isNull(value))
                value = this.get(key);
            if (Objects.isNull(value))
                return;
            try {
                field.setAccessible(true);
                field.set(this, value);
                field.setAccessible(false);
            } catch (ReflectiveOperationException | IllegalArgumentException e) {
                String configName = getClass().getName();
                throw new IllegalStateException(String.format(
                        "No serializer found for class %s (%s) in %s",
                        field.getType().getName(), key, configName
                ), e);
            }
        });
    }

    private Map<String, Field> getAnnotatedFields() {
        return Arrays.stream(this.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Configurable.class))
                .filter(field -> Objects.nonNull(field.getAnnotation(Configurable.class)))
                .collect(Collectors.toMap(field -> StringUtils.isEmpty(field.getAnnotation(Configurable.class)
                        .key()) ? FieldNameFormatters.LOWER_UNDERSCORE.apply(field.getName())
                        : field.getAnnotation(Configurable.class).key(), o -> o));
    }
}
