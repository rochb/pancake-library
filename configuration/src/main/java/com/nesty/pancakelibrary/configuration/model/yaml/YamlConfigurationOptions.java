package com.nesty.pancakelibrary.configuration.model.yaml;

import com.nesty.pancakelibrary.commons.utils.Validate;
import com.nesty.pancakelibrary.configuration.model.file.FileConfigurationOptions;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class YamlConfigurationOptions extends FileConfigurationOptions {

    private int indent = 2;

    protected YamlConfigurationOptions(@NonNull YamlConfiguration configuration) {
        super(configuration);
    }

    @NonNull
    @Override
    public YamlConfiguration getConfiguration() {
        return (YamlConfiguration) super.getConfiguration();
    }

    /**
     * Gets how much spaces should be used to indent each line.
     * <p>
     * The minimum value this may be is 2, and the maximum is 9.
     *
     * @return How much to indent by
     */
    public int indent() {
        return indent;
    }

    /**
     * Sets how many spaces should be used to indent each line.
     * <p>
     * The minimum value this may be is 2, and the maximum is 9.
     *
     * @param value New indent
     * @return This object, for chaining
     */
    @NonNull
    public YamlConfigurationOptions indent(int value) {
        Validate.isTrue(value >= 2, "Indent must be at least 2 characters");
        Validate.isTrue(value <= 9, "Indent cannot be greater than 9 characters");

        this.indent = value;
        return this;
    }
}