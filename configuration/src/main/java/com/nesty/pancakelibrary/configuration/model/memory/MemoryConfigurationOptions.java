package com.nesty.pancakelibrary.configuration.model.memory;

import com.nesty.pancakelibrary.configuration.model.ConfigurationOptions;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * Various settings for controlling the input and output of a {@link
 * MemoryConfiguration}
 */
public class MemoryConfigurationOptions extends ConfigurationOptions {

    protected MemoryConfigurationOptions(@NonNull MemoryConfiguration configuration) {
        super(configuration);
    }

    @NonNull
    @Override
    public MemoryConfiguration getConfiguration() {
        return (MemoryConfiguration) super.getConfiguration();
    }
}