package com.nesty.pancakelibrary.configuration.model.yaml;

import com.nesty.pancakelibrary.commons.utils.Validate;
import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.configuration.exceptions.InvalidConfigurationException;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import com.nesty.pancakelibrary.configuration.model.file.FileConfiguration;
import lombok.NonNull;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.representer.Representer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * An implementation of {@link Configuration} which saves all files in Yaml.
 * Note that this implementation is not synchronized.
 */
public class YamlConfiguration extends FileConfiguration {

    protected static final String COMMENT_PREFIX = "# ";
    protected static final String BLANK_CONFIG = "{}\n";
    private final DumperOptions yamlOptions = new DumperOptions();
    private final LoaderOptions loaderOptions = new LoaderOptions();
    private final Representer yamlRepresenter = new YamlRepresenter();
    private final Yaml yaml = new Yaml(new YamlConstructor(), yamlRepresenter, yamlOptions, loaderOptions);

    public YamlConfiguration(@NonNull File file) {
        super(file);
    }

    @NonNull
    @Override
    public String saveToString() {
        yamlOptions.setIndent(options().indent());
        yamlOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        yamlRepresenter.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        String dump = yaml.dump(getValues(false));
        if (dump.equals(BLANK_CONFIG))
            dump = "";
        return dump;
    }

    @Override
    public void loadFromString(@NonNull String contents) throws InvalidConfigurationException {
        Validate.notNull(contents, "Contents cannot be null");

        Map<?, ?> input;
        try {
            loaderOptions.setMaxAliasesForCollections(Integer.MAX_VALUE);
            input = yaml.load(contents);
        } catch (YAMLException e) {
            throw new InvalidConfigurationException(e);
        } catch (ClassCastException e) {
            throw new InvalidConfigurationException("Top level is not a Map.");
        }

        // String header = parseHeader(contents);
        // if (header.length() > 0)
        //     options().header(header);

        this.map.clear();

        if (input != null)
            convertMapsToSections(input, this);
    }

    protected void convertMapsToSections(@NonNull Map<?, ?> input, @NonNull ConfigurationSection section) {
        for (Map.Entry<?, ?> entry : input.entrySet()) {
            String key = entry.getKey().toString();
            Object value = entry.getValue();

            if (value instanceof Map)
                convertMapsToSections((Map<?, ?>) value, section.createSection(key));
            else
                section.set(key, value);
        }
    }

    @NonNull
    protected String parseHeader(@NonNull String input) {
        String[] lines = input.split("\r?\n", -1);
        StringBuilder result = new StringBuilder();
        boolean readingHeader = true;
        boolean foundHeader = false;

        for (int i = 0; (i < lines.length) && (readingHeader); i++) {
            String line = lines[i];

            if (line.startsWith(COMMENT_PREFIX)) {
                if (i > 0)
                    result.append("\n");

                if (line.length() > COMMENT_PREFIX.length())
                    result.append(line.substring(COMMENT_PREFIX.length()));
                foundHeader = true;
            } else if ((foundHeader) && (line.length() == 0))
                result.append("\n");
            else if (foundHeader)
                readingHeader = false;
        }
        return result.toString();
    }

    @NonNull
    @Override
    public YamlConfigurationOptions options() {
        if (options == null)
            options = new YamlConfigurationOptions(this);
        return (YamlConfigurationOptions) options;
    }

    /**
     * Creates a new {@link YamlConfiguration}, loading from the given file.
     * <p>
     * Any errors loading the Configuration will be logged and then ignored.
     * If the specified input is not a valid config, a blank config will be
     * returned.
     * <p>
     * The encoding used may follow the system dependent default.
     *
     * @param file Input file
     * @return Resulting configuration
     * @throws IllegalArgumentException Thrown if file is null
     */
    @NonNull
    public static YamlConfiguration loadConfiguration(@NonNull File file) {
        Validate.notNull(file, "File cannot be null");

        YamlConfiguration config = new YamlConfiguration(file);

        try {
            config.load();
        } catch (FileNotFoundException ignored) {
        } catch (IOException | InvalidConfigurationException ex) {
            PancakeLogger.get().error("Cannot load %s configuration file!", file.getName());
            ex.printStackTrace();
        }
        return config;
    }
}