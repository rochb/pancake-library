package com.nesty.pancakelibrary.configuration.model.yaml;

import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import lombok.NonNull;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.representer.Representer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class YamlRepresenter extends Representer {

    public YamlRepresenter() {
        this.multiRepresenters.put(ConfigurationSection.class, new RepresentConfigurationSection());
        this.multiRepresenters.remove(Enum.class);
    }

    private class RepresentConfigurationSection extends RepresentMap {

        @NonNull
        @Override
        public Node representData(@NonNull Object data) {
            return super.representData(((ConfigurationSection) data).getValues(false));
        }
    }
}
