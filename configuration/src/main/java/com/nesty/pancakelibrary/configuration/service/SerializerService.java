package com.nesty.pancakelibrary.configuration.service;

import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import com.nesty.pancakelibrary.configuration.model.serializer.ConfigurationSerializer;
import com.nesty.pancakelibrary.configuration.repository.SerializerRepository;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class SerializerService {

    private final SerializerRepository repository;

    public SerializerService() {
        this.repository = new SerializerRepository();
    }

    public void add(@NonNull ConfigurationSerializer<?> serializer) {
        this.repository.add(serializer);
    }

    public void add(@NonNull Class<? extends ConfigurationSerializer<?>>... classes) {
        for (Class<? extends ConfigurationSerializer<?>> aClass : classes)
            add(aClass);
    }

    public void add(@NonNull Class<? extends ConfigurationSerializer<?>> clazz) {
        if (isRegistered(clazz)) {
            PancakeLogger.get().warn("%s is already registered!", clazz.getName());
            return;
        }
        try {
            add(clazz.newInstance());
        } catch (Exception e) {
            PancakeLogger.get().error("Cannot register %s configuration serializer!", clazz.getName());
            e.printStackTrace();
        }
    }

    public boolean isRegistered(@NonNull Class<?> clazz) {
        return getByTarget(clazz).isPresent()
                || getByClass(clazz).isPresent();
    }

    public void remove(@NonNull ConfigurationSerializer<?> serializer) {
        this.repository.remove(serializer);
    }

    public Optional<ConfigurationSerializer<?>> getByTarget(@NonNull Class<?> clazz) {
        return this.repository.getByTarget(clazz);
    }

    public Optional<ConfigurationSerializer<?>> getByClass(@NonNull Class<?> clazz) {
        return this.repository.getByClass(clazz);
    }

    public List<ConfigurationSerializer<?>> getSerializers() {
        return this.repository.getSerializers();
    }

    public Optional<Object> deserialize(@NonNull Configuration configuration, @NonNull ConfigurationSection section, @NonNull Class<?> type) {
        return getByTarget(type)
                .flatMap(serializer -> serializer.deserialize(configuration, section.getCurrentPath()));
    }

    public Optional<Object> deserialize(@NonNull Configuration configuration, @NonNull String key, @NonNull Class<?> type) {
        return getByTarget(type)
                .flatMap(serializer -> serializer.deserialize(configuration, key));
    }

    public Object deserializeList(@NonNull Configuration configuration, @NonNull ConfigurationSection section, @NonNull Class<?> type) {
        return getByTarget(type)
                .map(serializer -> serializer.deserializeList(configuration, section.getCurrentPath()));
    }

}
