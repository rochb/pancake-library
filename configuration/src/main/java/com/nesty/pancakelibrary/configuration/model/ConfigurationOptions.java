package com.nesty.pancakelibrary.configuration.model;

import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * Various settings for controlling the input and output of a {@link
 * Configuration}
 */
@Data
@Setter
public class ConfigurationOptions {

    private char pathSeparator = '.';
    private final Configuration configuration;

    protected ConfigurationOptions(@NonNull Configuration configuration) {
        this.configuration = configuration;
    }
}