package com.nesty.pancakelibrary.configuration.model.yaml;

import com.nesty.pancakelibrary.configuration.exceptions.InvalidConfigurationException;
import com.nesty.pancakelibrary.configuration.model.utils.TestUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class YamlConfigurationTest {

    static YamlConfiguration configuration;
    static File file;

    @BeforeAll
    static void init() throws IOException {
        file = new File("src/test/resources/test.yml");
        if (file.exists())
            afterAll();
        Files.copy(new File(file.getParent(), "test-copy.yml").toPath(), file.toPath());
        configuration = new YamlConfiguration(file);
    }

    @Order(1)
    @Test
    void testLoad() throws IOException, InvalidConfigurationException {
        configuration.load();
        assert configuration.isConfigurationSection("section");
    }

    @Order(2)
    @Test
    void testSave() throws IOException {
        assert !configuration.isSet("nb");
        assert TestUtils.getValue(file, "nb") == null;
        configuration.set("nb", "oooo");
        assert configuration.isSet("nb");
        assert configuration.isString("nb");
        configuration.save();
        assert TestUtils.getValue(file, "nb") != null;
    }

    @Order(3)
    @Test
    void deleteValueAndSave() throws IOException {
        assert configuration.isSet("nb");
        assert configuration.isString("nb");
        configuration.set("nb", null);
        assert !configuration.isSet("nb");
        configuration.save();
        assert TestUtils.getValue(file, "nb") == null;
    }

    @AfterAll
    static void afterAll() throws IOException {
        Files.delete(file.toPath());
    }
}