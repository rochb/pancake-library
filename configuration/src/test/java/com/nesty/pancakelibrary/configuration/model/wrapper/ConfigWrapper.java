package com.nesty.pancakelibrary.configuration.model.wrapper;

import com.nesty.pancakelibrary.configuration.model.TestModel;
import com.nesty.pancakelibrary.configuration.model.annotation.Configurable;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class ConfigWrapper extends YamlConfigurationWrapper {

    @Configurable
    private String test;

    @Configurable(key = "id")
    private UUID uniqueId;

    @Configurable(key = "model")
    private TestModel validModel;

    @Configurable(key = "invalid-model")
    private TestModel invalidModel;

    public ConfigWrapper() {
        super(new File("src/test/resources/wrapper.yml"));
    }

}
