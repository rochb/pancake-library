package com.nesty.pancakelibrary.configuration.service;

import com.nesty.pancakelibrary.configuration.model.serializer.UUIDSerializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class SerializerServiceTest {

    static SerializerService service;

    @BeforeAll
    static void beforeAll() {
        service = new SerializerService();
    }

    @BeforeEach
    void setUp() {
        service.getSerializers().clear();
    }

    @Test
    void registerSerializer() {
        assert !service.isRegistered(UUIDSerializer.class);
        service.add(UUIDSerializer.class);
        assert service.isRegistered(UUIDSerializer.class);
    }

    @Test
    void registerSerializerTwice() {
        assert !service.isRegistered(UUIDSerializer.class);
        service.add(UUIDSerializer.class);
        assert service.isRegistered(UUIDSerializer.class);
        service.add(UUIDSerializer.class);
        assert service.getSerializers().size() == 1;
    }

    @Test
    void getSerializerByClass() {
        assert !service.isRegistered(UUIDSerializer.class);
        service.add(UUIDSerializer.class);
        assert service.getByClass(UUIDSerializer.class).isPresent();
    }

    @Test
    void getSerializerByTarget() {
        assert !service.isRegistered(UUIDSerializer.class);
        service.add(UUIDSerializer.class);
        assert service.getByTarget(UUID.class).isPresent();
    }
}