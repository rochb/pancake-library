package com.nesty.pancakelibrary.configuration.model.serializer;

import com.nesty.pancakelibrary.commons.utils.Validate;
import com.nesty.pancakelibrary.configuration.exceptions.InvalidValueException;
import com.nesty.pancakelibrary.configuration.model.Configuration;
import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import com.nesty.pancakelibrary.configuration.model.TestModel;
import lombok.NonNull;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TestModelSerializer implements ConfigurationSerializer<TestModel> {

    @Override
    public Optional<TestModel> deserialize(@NonNull Configuration configuration, @NonNull String key) throws InvalidValueException {
        if (!configuration.isConfigurationSection(key))
            throw new InvalidValueException(String.format("%s is not a configuration section!", key));
        ConfigurationSection section = configuration.getConfigurationSection(key);
        Validate.isTrue(section.isString("name"), "%s.name is not a string!", key);
        Validate.isTrue(section.isString("description"), "%s.description is not a string!", key);
        Validate.isTrue(section.isBoolean("valid"), "%s.valid is not a boolean!", key);
        Validate.isTrue(section.isInt("speed"), "%s.valid is not an integer!", key);
        String name = section.getString("name");
        String desc = section.getString("description");
        boolean valid = section.getBoolean("valid");
        int speed = section.getInt("speed");
        return Optional.of(new TestModel(name, desc, valid, speed));
    }

    @Override
    public void serialize(@NonNull Configuration configuration, @NonNull String key, @NonNull Object object) throws InvalidValueException {
        TestModel model = (TestModel) object;
        ConfigurationSection section = configuration.createSection(key);
        section.set("name", model.getName());
        section.set("description", model.getDescription());
        section.set("valid", model.isValid());
        section.set("speed", model.getSpeed());
    }
}
