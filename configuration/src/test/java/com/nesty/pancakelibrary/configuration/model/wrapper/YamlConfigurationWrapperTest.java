package com.nesty.pancakelibrary.configuration.model.wrapper;

import com.nesty.pancakelibrary.configuration.exceptions.InvalidConfigurationException;
import com.nesty.pancakelibrary.configuration.exceptions.InvalidValueException;
import com.nesty.pancakelibrary.configuration.model.serializer.TestModelSerializer;
import com.nesty.pancakelibrary.configuration.model.serializer.UUIDSerializer;
import com.nesty.pancakelibrary.configuration.model.utils.TestUtils;
import com.nesty.pancakelibrary.configuration.service.SerializerService;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class YamlConfigurationWrapperTest {

    static ConfigWrapper wrapper;
    static SerializerService serializerService;

    @BeforeAll
    static void beforeAll() throws IOException {
        wrapper = new ConfigWrapper();
        serializerService = new SerializerService();
        serializerService.add(UUIDSerializer.class, TestModelSerializer.class);
        wrapper.setSerializerService(serializerService);
        if (wrapper.getFile().exists())
            afterAll();
        Files.copy(new File(wrapper.getFile().getParent(), "wrapper-copy.yml").toPath(), wrapper.getFile().toPath());
    }

    @Test
    @Order(1)
    void testLoad() throws IOException, InvalidConfigurationException {
        wrapper.load();
    }

    @Test
    @Order(2)
    void testLoadedValues() {
        assert wrapper.getTest() != null;
        assert wrapper.getUniqueId() != null;
        assert wrapper.getTest().equals("a");
    }

    @Test
    @Order(3)
    void testSaveValues() throws IOException {
        assert wrapper.getTest() != null;
        assert wrapper.getTest().equals("a");
        assert wrapper.getValidModel() != null;
        assert wrapper.getValidModel().isValid();
        assert !wrapper.getValidModel().getName().isEmpty();
        assert Objects.equals(TestUtils.getValue(wrapper.getFile(), "test"), "test: a");
        wrapper.setTest("b");
        wrapper.save();
        assert wrapper.getTest() != null;
        assert wrapper.getTest().equals("b");
        assert Objects.equals(TestUtils.getValue(wrapper.getFile(), "test"), "test: b");
        assert Objects.equals(TestUtils.getValue(wrapper.getFile(), "id"), "id: f8c3de3d-1fea-4d7c-a8b0-29f63c4c3454");
    }

    @Test
    @Order(4)
    void testInvalidValues() throws IOException, InvalidConfigurationException {
        TestUtils.setValue(wrapper.getFile(), "id", "f8c3de3===-a8b029f63c");
        Assertions.assertThrows(InvalidValueException.class, () -> wrapper.load());
        TestUtils.setValue(wrapper.getFile(), "test", "1");
        Assertions.assertThrows(IllegalStateException.class, () -> wrapper.load());
    }

    @AfterAll
    static void afterAll() throws IOException {
        Files.delete(wrapper.getFile().toPath());
    }
}