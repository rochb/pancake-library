package com.nesty.pancakelibrary.configuration.model.utils;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@UtilityClass
public class TestUtils {

    @SneakyThrows
    public static String getValue(@NonNull File file, @NonNull String key) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = br.readLine()) != null)
                lines.add(line);
        }
        return lines.stream()
                .filter(s -> s.contains(key + ":"))
                .findFirst()
                .orElse(null);
    }

    @SneakyThrows
    public static void setValue(@NonNull File file, @NonNull String key, String value) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = br.readLine()) != null)
                lines.add(line);
        }
        lines.stream()
                .filter(s -> s.contains(key + ":"))
                .findFirst()
                .ifPresent(s -> {
                    int index = lines.indexOf(s);
                    lines.remove(s);
                    s = s.substring(0, s.lastIndexOf(":") + 1);
                    s += " ";
                    s += value;
                    lines.add(s);
                });
        PrintWriter pw = new PrintWriter(new FileWriter(file));
        lines.forEach(pw::println);
        pw.close();
    }
}
