package com.nesty.pancakelibrary.configuration.model.serializer;

import com.nesty.pancakelibrary.configuration.model.memory.MemoryConfiguration;
import org.junit.jupiter.api.*;

import java.util.Optional;
import java.util.UUID;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UUIDSerializerTest {

    static MemoryConfiguration configuration;
    static UUID uuid;
    static UUIDSerializer serializer;
    static String key = "test-unique-id";

    @BeforeAll
    static void beforeAll() {
        configuration = new MemoryConfiguration();
        uuid = UUID.randomUUID();
        serializer = new UUIDSerializer();
    }

    @Order(1)
    @Test
    void serialize() {
        assert !configuration.isSet(key);
        serializer.serialize(configuration, key, uuid);
        assert configuration.isSet(key);
        assert configuration.isString(key);
        assert configuration.getString(key).equals(uuid.toString());
    }

    @Order(2)
    @Test
    void deserialize() {
        assert configuration.isSet(key);
        assert configuration.isString(key);
        Optional<UUID> result = serializer.deserialize(configuration, key);
        Optional<UUID> result1 = serializer.deserialize(configuration, key + "zezeaze");
        assert result.isPresent();
        assert !result1.isPresent();
        assert result.get().equals(uuid);
    }
}