package com.nesty.pancakelibrary.configuration.model.memory;

import com.nesty.pancakelibrary.configuration.model.ConfigurationSection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Arrays;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MemoryConfigurationTest {

    private static MemoryConfiguration configuration;

    @BeforeAll
    static void init() {
        configuration = new MemoryConfiguration();
    }

    @Test
    void testStrings() {
        configuration.set("s-1", "a");
        assert configuration.isSet("s-1");
        assert configuration.isString("s-1");
        assert configuration.get("s-1") instanceof String;
        assert configuration.getString("s-1").equals("a");
    }

    @Test
    void testDoubles() {
        configuration.set("d-1", 1.0);
        assert configuration.isSet("d-1");
        assert configuration.isDouble("d-1");
        assert configuration.get("d-1") instanceof Double;
        assert configuration.getDouble("d-1") == 1.0;
    }

    @Test
    void testIntegers() {
        configuration.set("i-1", 1654);
        assert configuration.isSet("i-1");
        assert configuration.isInt("i-1");
        assert configuration.get("i-1") instanceof Integer;
        assert configuration.getDouble("i-1") == 1654;
    }

    @Test
    void testLists() {
        configuration.set("l-1", Arrays.asList("a", "b", "c"));
        assert configuration.isSet("l-1");
        assert configuration.isList("l-1");
        assert configuration.get("l-1") instanceof List;
        assert configuration.getStringList("l-1").get(0).equals("a");
        assert configuration.getStringList("l-1").get(1).equals("b");
        assert configuration.getStringList("l-1").get(2).equals("c");
    }

    @Test
    void testSections() {
        ConfigurationSection section = configuration.createSection("test-section");
        section.set("a", "a");
        section.set("b", 1.0);
        section.set("c", 1);
        section.set("d", true);
        assert configuration.isSet("test-section");
        assert configuration.isConfigurationSection("test-section");
        assert configuration.getConfigurationSection("test-section").isString("a");
        assert configuration.getConfigurationSection("test-section").isDouble("b");
        assert configuration.getConfigurationSection("test-section").isInt("c");
        assert configuration.getConfigurationSection("test-section").isBoolean("d");
    }
}