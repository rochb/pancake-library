package com.nesty.pancakelibrary.configuration.model;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class TestModel {

    private final String name;
    private final String description;
    private final boolean valid;
    private final int speed;
}
