package com.nesty.pancakelibrary.configuration;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.configuration.model.serializer.TestModelSerializer;
import com.nesty.pancakelibrary.configuration.model.utils.TestUtils;
import com.nesty.pancakelibrary.configuration.model.wrapper.ConfigWrapper;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfigurationModuleTest {

    static ConfigurationModule module;
    static ConfigWrapper wrapper;
    static File file;

    @BeforeAll
    static void beforeAll() throws IOException {
        module = new ConfigurationModule();
        module.load(new PancakePlugin() {
            @Override
            public String getName() {
                return "test";
            }

            @Override
            public File getDataFolder() {
                return null;
            }
        });
        module.getSerializerService().add(TestModelSerializer.class);
        file = new File("src/test/resources/wrapper.yml");

        if (file.exists())
            afterAll();
        Files.copy(new File(file.getParent(), "wrapper-copy.yml").toPath(), file.toPath());
    }

    @Test
    @Order(1)
    void registerWrappedConfiguration() {
        wrapper = module.register(new ConfigWrapper());
        assert wrapper != null;
    }

    @Test
    @Order(2)
    void testWrappedConfigurationValues() {
        assert wrapper.getTest() != null;
        assert wrapper.getUniqueId() != null;
        assert wrapper.getValidModel() != null;
        assert wrapper.getTest().equals("a");
        assert wrapper.getValidModel().isValid();
        assert wrapper.getValidModel().getName().equals("Model #1");
    }

    @Test
    @Order(3)
    void testReload() {
        TestUtils.setValue(wrapper.getFile(), "test", "b");
        UUID id = UUID.randomUUID();
        TestUtils.setValue(wrapper.getFile(), "id", id.toString());
        assert wrapper.getTest().equals("a");
        assert !wrapper.getUniqueId().equals(id);
        module.reload();
        assert wrapper.getTest().equals("b");
        assert wrapper.getUniqueId().equals(id);
    }

    @AfterAll
    static void afterAll() throws IOException {
        Files.delete(file.toPath());
    }
}