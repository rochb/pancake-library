package net.nesty.pancakelibrary.tasks.model;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface TaskService {

    TaskFactory getFactory();

    TimerFactory getTimerFactory();

}
