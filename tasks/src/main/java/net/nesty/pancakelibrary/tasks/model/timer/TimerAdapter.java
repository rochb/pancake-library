package net.nesty.pancakelibrary.tasks.model.timer;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingTask;

@RequiredArgsConstructor
public class TimerAdapter implements Timer {

    private final RepeatingTask delegate;

    @Override
    public void start() {
        delegate.start();
    }

    @Override
    public void stop() {
        delegate.stop();
    }
}
