package net.nesty.pancakelibrary.tasks.model.timer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.concurrent.TimeUnit;

@Getter
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class TaskOptions {

    private final TimeUnit timeUnit;
    private final int interval;
    private final long timesToRun;

    public TaskOptions(int timesToRun) {
        this(TimeUnit.SECONDS, timesToRun);
    }

    private TaskOptions(TimeUnit timeUnit, int timesToRun) {
        this(timeUnit, 1, timesToRun);
    }

}
