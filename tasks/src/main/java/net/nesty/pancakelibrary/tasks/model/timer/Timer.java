package net.nesty.pancakelibrary.tasks.model.timer;

import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingTask;

/**
 * {@inheritDoc}
 */
public interface Timer extends RepeatingTask {

    /**
     * {@inheritDoc}
     */
    void start();

    /**
     * {@inheritDoc}
     */
    void stop();
}
