package net.nesty.pancakelibrary.tasks.model;

public interface Task {

    void execute();
}
