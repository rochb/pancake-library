package net.nesty.pancakelibrary.tasks.model.startup;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for executing post-load tasks.
 */
public class PostLoadTaskRunner {

    private final List<PostLoadTask> tasks = new ArrayList<>();

    public void add(PostLoadTask task) {
        this.tasks.add(task);
    }

    public void run() {
        this.tasks.forEach(PostLoadTask::run);
    }
}
