package net.nesty.pancakelibrary.tasks.model;

import lombok.RequiredArgsConstructor;

import java.util.Set;

/**
 * Class responsible for starting fixed repeating task after the server startup.
 */
@RequiredArgsConstructor
public class FixedRepeatingTaskRunner {

    private final TaskFactory taskFactory;

    public void runTasks(Set<FixedRepeatingAction> actionSet) {
        actionSet.forEach(fixedRepeatingAction -> taskFactory
                .newRepeatingTask(fixedRepeatingAction, fixedRepeatingAction.getInterval())
                .start());
    }
}
