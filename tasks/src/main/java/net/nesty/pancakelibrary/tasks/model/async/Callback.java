package net.nesty.pancakelibrary.tasks.model.async;

public interface Callback<T> {

    void call(T t);
}
