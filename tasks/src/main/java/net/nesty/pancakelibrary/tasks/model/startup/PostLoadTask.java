package net.nesty.pancakelibrary.tasks.model.startup;

/**
 * A task that runs right after the server startup. Players are not able to join
 * the server until all tasks are completed.
 */
public interface PostLoadTask {

    void run();

}
