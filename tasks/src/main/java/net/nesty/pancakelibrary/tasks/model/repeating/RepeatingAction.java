package net.nesty.pancakelibrary.tasks.model.repeating;

import net.nesty.pancakelibrary.tasks.model.SchedulerTask;

public interface RepeatingAction {

    /**
     * Tact of the task. This method represents the repeating
     * action each self.
     *
     * @param owner parent task, i.e action executor
     */
    void run(SchedulerTask owner);

}
