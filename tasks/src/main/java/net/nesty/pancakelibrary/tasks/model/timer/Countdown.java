package net.nesty.pancakelibrary.tasks.model.timer;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.SchedulerTask;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingAction;

@RequiredArgsConstructor
public class Countdown implements RepeatingAction {

    private final CountdownTask countdownTask;
    private final TaskOptions taskOptions;

    private long timesExecuted;

    @Override
    public void run(SchedulerTask owner) {
        countdownTask.onTick(++timesExecuted);
        if (timesExecuted == taskOptions.getTimesToRun()) {
            countdownTask.complete();
            owner.cancel();
        }
    }

}
