package net.nesty.pancakelibrary.tasks.model.repeating;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.SchedulerTask;

@RequiredArgsConstructor
public class RepeatingRunnable implements RepeatingAction {

    private final Runnable runnable;

    @Override
    public void run(SchedulerTask owner) {
        this.runnable.run();
    }
}
