package net.nesty.pancakelibrary.tasks.model;

import net.nesty.pancakelibrary.tasks.model.timer.CountdownTask;
import net.nesty.pancakelibrary.tasks.model.timer.TaskOptions;
import net.nesty.pancakelibrary.tasks.model.timer.Timer;

public interface TimerFactory {

    Timer newTimer(CountdownTask countdownTask, TaskOptions taskOptions);
}
