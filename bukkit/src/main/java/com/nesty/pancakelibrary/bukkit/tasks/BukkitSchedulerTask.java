package com.nesty.pancakelibrary.bukkit.tasks;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;
import net.nesty.pancakelibrary.tasks.model.SchedulerTask;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingAction;
import org.bukkit.scheduler.BukkitScheduler;

@RequiredArgsConstructor
public class BukkitSchedulerTask implements SchedulerTask {

    private final BukkitScheduler bukkitScheduler;
    private final ExecutionStrategy repeatingStrategy;
    private final RepeatingAction repeatingAction;
    private int bukkitTaskId = -1;

    @Override
    public int getTaskId() {
        return bukkitTaskId;
    }

    @Override
    public void cancel() {
        bukkitScheduler.cancelTask(bukkitTaskId);
    }

    @Override
    public void start() {
        this.bukkitTaskId = repeatingStrategy.execute(() -> repeatingAction.run(this));
    }
}
