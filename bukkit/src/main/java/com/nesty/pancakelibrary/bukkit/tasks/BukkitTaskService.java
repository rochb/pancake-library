package com.nesty.pancakelibrary.bukkit.tasks;

import lombok.Getter;
import net.nesty.pancakelibrary.tasks.model.TaskService;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

@Getter
public class BukkitTaskService implements TaskService {

    private final BukkitTaskFactory factory;
    private final BukkitTimerFactory timerFactory;

    public BukkitTaskService(Plugin plugin) {
        BukkitScheduler scheduler = plugin.getServer().getScheduler();
        this.factory = new BukkitTaskFactory(plugin, scheduler);
        this.timerFactory = new BukkitTimerFactory(plugin, scheduler);
    }
}
