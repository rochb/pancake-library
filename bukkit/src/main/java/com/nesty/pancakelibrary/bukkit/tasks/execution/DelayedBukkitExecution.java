package com.nesty.pancakelibrary.bukkit.tasks.execution;

import com.nesty.pancakelibrary.bukkit.tasks.execution.delayed.DelayedExecution;
import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;
import net.nesty.pancakelibrary.tasks.util.SecondsToTicks;
import org.bukkit.plugin.Plugin;

@RequiredArgsConstructor
public class DelayedBukkitExecution implements ExecutionStrategy {

    private final Plugin plugin;
    private final DelayedExecution delayedExecution;
    private final int delayInSeconds;

    @Override
    public int execute(Runnable runnable) {
        return delayedExecution.execute(plugin, plugin.getServer().getScheduler(), runnable, new SecondsToTicks(delayInSeconds).convert()).getTaskId();
    }
}
