package com.nesty.pancakelibrary.bukkit;

import org.bukkit.plugin.java.JavaPlugin;

public final class Bukkit extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
