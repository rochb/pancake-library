package com.nesty.pancakelibrary.bukkit.tasks;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.async.Callback;
import net.nesty.pancakelibrary.tasks.model.type.CompletableTask;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.function.BiConsumer;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class AsyncFunction<T> implements CompletableTask<T> {

    private final Supplier<T> job;
    private final Plugin plugin;
    private Callback<T> successHandler;

    @Override
    public void completeAsynchronously(Callback<T> successHandler) {
        this.complete((bukkitScheduler, runnable) -> bukkitScheduler.runTaskAsynchronously(plugin, runnable));
        this.successHandler = successHandler;
    }

    @Override
    public void completeSynchronously(Callback<T> successHandler) {
        this.complete((bukkitScheduler, runnable) -> bukkitScheduler.runTask(plugin, runnable));
        this.successHandler = successHandler;
    }

    private void complete(BiConsumer<BukkitScheduler, Runnable> executionPlan) {
        final BukkitScheduler scheduler = plugin.getServer().getScheduler();
        scheduler.runTaskAsynchronously(plugin, () -> {
            final T taskResult = job.get();
            executionPlan.accept(scheduler, () -> successHandler.call(taskResult));
        });
    }

}
