package com.nesty.pancakelibrary.bukkit.tasks;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.Task;
import org.bukkit.plugin.Plugin;

@RequiredArgsConstructor
public class AsyncProcedure implements Task {

    private final Runnable job;
    private final Plugin plugin;

    @Override
    public void execute() {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, job);
    }
}
