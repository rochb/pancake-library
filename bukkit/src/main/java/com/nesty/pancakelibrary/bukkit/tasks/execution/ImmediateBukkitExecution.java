package com.nesty.pancakelibrary.bukkit.tasks.execution;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

@RequiredArgsConstructor
public class ImmediateBukkitExecution implements ExecutionStrategy {

    private final Plugin plugin;
    private final BukkitScheduler bukkitScheduler;

    @Override
    public int execute(Runnable runnable) {
        return bukkitScheduler.runTask(plugin, runnable).getTaskId();
    }
}
