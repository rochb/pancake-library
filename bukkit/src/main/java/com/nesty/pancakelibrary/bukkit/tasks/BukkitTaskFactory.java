package com.nesty.pancakelibrary.bukkit.tasks;

import com.nesty.pancakelibrary.bukkit.tasks.execution.DelayedBukkitExecution;
import com.nesty.pancakelibrary.bukkit.tasks.execution.ImmediateBukkitExecution;
import com.nesty.pancakelibrary.bukkit.tasks.execution.RepeatingBukkitExecution;
import com.nesty.pancakelibrary.bukkit.tasks.execution.delayed.AsyncDelayedExecution;
import com.nesty.pancakelibrary.bukkit.tasks.execution.delayed.DelayedExecution;
import com.nesty.pancakelibrary.bukkit.tasks.execution.delayed.SyncDelayedExecution;
import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.AbstractRepeatingTask;
import net.nesty.pancakelibrary.tasks.model.Task;
import net.nesty.pancakelibrary.tasks.model.TaskFactory;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingRunnable;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingTask;
import net.nesty.pancakelibrary.tasks.model.type.CompletableTask;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.function.Supplier;

@RequiredArgsConstructor
public class BukkitTaskFactory implements TaskFactory {

    private final Plugin plugin;
    private final BukkitScheduler bukkitScheduler;

    @Override
    public Task newSyncTask(Runnable runnable) {
        Task task = new DelegatingTask(runnable, new ImmediateBukkitExecution(plugin, bukkitScheduler));
        task.execute();
        return task;
    }

    @Override
    public Task newAsyncDelayedTask(Runnable runnable, int delayInSeconds) {
        Task task = newDelayedTask(runnable, new AsyncDelayedExecution(), delayInSeconds);
        task.execute();
        return task;
    }

    @Override
    public Task newSyncDelayedTask(Runnable runnable, int delayInSeconds) {
        Task task = newDelayedTask(runnable, new SyncDelayedExecution(), delayInSeconds);
        task.execute();
        return task;
    }

    @Override
    public <R> CompletableTask<R> newAsyncFunction(Supplier<R> task) {
        return new AsyncFunction<>(task, plugin);
    }

    @Override
    public Task newAsyncTask(Runnable runnable) {
        Task task = new AsyncProcedure(runnable, plugin);
        task.execute();
        return task;
    }

    @Override
    public RepeatingTask newRepeatingTask(Runnable runnable, int period) {
        RepeatingTask task = new AbstractRepeatingTask(
                new BukkitSchedulerTask(
                        bukkitScheduler,
                        new RepeatingBukkitExecution(plugin, bukkitScheduler, period),
                        new RepeatingRunnable(runnable)
                )
        );
        task.start();
        return task;
    }

    private Task newDelayedTask(Runnable runnable, DelayedExecution delayedExecution, int delay) {
        return new DelegatingTask(runnable, new DelayedBukkitExecution(plugin, delayedExecution, delay));
    }
}
