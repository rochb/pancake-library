package com.nesty.pancakelibrary.bukkit.tasks.execution;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;
import net.nesty.pancakelibrary.tasks.util.SecondsToTicks;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

@RequiredArgsConstructor
public class RepeatingBukkitExecution implements ExecutionStrategy {

    private final Plugin plugin;
    private final BukkitScheduler bukkitScheduler;
    private final int delay;

    @Override
    public int execute(Runnable runnable) {
        return bukkitScheduler.runTaskTimer(plugin, runnable, 0L, new SecondsToTicks(delay).convert()).getTaskId();
    }
}
