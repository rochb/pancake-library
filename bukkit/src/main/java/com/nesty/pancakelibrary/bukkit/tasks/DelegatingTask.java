package com.nesty.pancakelibrary.bukkit.tasks;

import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;
import net.nesty.pancakelibrary.tasks.model.Task;

@RequiredArgsConstructor
public class DelegatingTask implements Task {

    private final Runnable job;
    private final ExecutionStrategy executionStrategy;

    @Override
    public void execute() {
        executionStrategy.execute(job);
    }

}
