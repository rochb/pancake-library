package com.nesty.pancakelibrary.bukkit.tasks;

import com.nesty.pancakelibrary.bukkit.tasks.execution.RepeatingBukkitExecution;
import lombok.RequiredArgsConstructor;
import net.nesty.pancakelibrary.tasks.model.AbstractRepeatingTask;
import net.nesty.pancakelibrary.tasks.model.TimerFactory;
import net.nesty.pancakelibrary.tasks.model.repeating.ReusableRepeatingTask;
import net.nesty.pancakelibrary.tasks.model.timer.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

@RequiredArgsConstructor
public class BukkitTimerFactory implements TimerFactory {

    private final Plugin plugin;
    private final BukkitScheduler bukkitScheduler;

    @Override
    public Timer newTimer(CountdownTask countdownTask, TaskOptions taskOptions) {
        Timer timer = new TimerAdapter(
                new ReusableRepeatingTask(
                        () -> new AbstractRepeatingTask(
                                new BukkitSchedulerTask(
                                        bukkitScheduler,
                                        new RepeatingBukkitExecution(plugin, bukkitScheduler, taskOptions.getInterval()),
                                        new Countdown(countdownTask, taskOptions)
                                )
                        )
                )
        );
        timer.start();
        return timer;
    }
}
