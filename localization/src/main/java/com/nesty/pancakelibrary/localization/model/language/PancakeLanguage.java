package com.nesty.pancakelibrary.localization.model.language;

import lombok.NonNull;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PancakeLanguage {

    String getValue();

    TranslationNode getNode();

    Locale getLocale();

    List<TranslationNode> getNodes();

    default Language toStandardLanguage(@NonNull File dataFolder) {
        return new Language(getLocale(), getNodes(), new File(dataFolder, getLocale().toString() + ".yml"));
    }
}
