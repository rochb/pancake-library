package com.nesty.pancakelibrary.localization.repository;

import com.nesty.pancakelibrary.localization.model.language.Language;
import com.nesty.pancakelibrary.localization.model.language.TranslationNode;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LanguageRepository {

    @Getter
    private final List<Language> languages;

    public LanguageRepository() {
        this.languages = new ArrayList<>();
    }

    public void add(@NonNull Language language) {
        this.languages.add(language);
    }

    public void remove(@NonNull Language language) {
        this.languages.remove(language);
    }

    public Optional<TranslationNode> getNode(@NonNull Locale locale, @NonNull String id) {
        return getByLocale(locale)
                .flatMap(language -> language.getNodes()
                .stream()
                .filter(translationNode -> translationNode.getId().equals(id))
                .findFirst());
    }

    public Optional<Language> getByLocale(@NonNull Locale locale) {
        return this.languages
                .stream()
                .filter(language -> language.getLocale().equals(locale))
                .findFirst();
    }
}
