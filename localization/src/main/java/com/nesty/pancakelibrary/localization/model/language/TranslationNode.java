package com.nesty.pancakelibrary.localization.model.language;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class TranslationNode {

    private final String id;
    private final String value;

}
