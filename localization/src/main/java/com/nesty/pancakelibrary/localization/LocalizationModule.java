package com.nesty.pancakelibrary.localization;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import com.nesty.pancakelibrary.localization.model.language.Language;
import com.nesty.pancakelibrary.localization.model.language.PancakeLanguage;
import com.nesty.pancakelibrary.localization.model.language.TranslationNode;
import com.nesty.pancakelibrary.localization.model.loader.LanguageLoader;
import com.nesty.pancakelibrary.localization.model.loader.YmlLanguageLoader;
import com.nesty.pancakelibrary.localization.repository.LanguageRepository;
import com.nesty.pancakelibrary.modules.CommonModules;
import com.nesty.pancakelibrary.modules.model.module.JavaModule;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import sun.misc.Unsafe;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class LocalizationModule extends JavaModule {

    private final LanguageRepository repository;

    @Getter
    private final File datafolder;

    @Setter
    private LanguageLoader loader;

    @Getter
    @Setter
    private Locale locale;

    public LocalizationModule(@NonNull File datafolder, @NonNull Locale locale) {
        super(CommonModules.LOCALIZATION.getName());
        this.repository = new LanguageRepository();
        this.datafolder = datafolder;
        this.loader = new YmlLanguageLoader();
        this.locale = locale;
    }

    @Override
    public void load(@NonNull PancakePlugin plugin) {
        logger.info("Loading languages...");
        loadSilently();
        logger.success("%d languages loaded!", repository.getLanguages().size());
    }

    @Override
    public void unload() {
        repository.getLanguages().clear();
    }

    private void loadSilently() {
        initDataFolder();
        repository.getLanguages().clear();
        loadFromDataFolder();
    }

    public void register(@NonNull Language language) {
        if (getByLocale(language.getLocale()).isPresent())
            logger.warn("Language %s is already registered!", language.getLocale().getDisplayLanguage(Locale.ROOT));
        else
            this.repository.add(language);
    }

    public void unregister(@NonNull Language language) {
        this.repository.remove(language);
    }

    public String translate(@NonNull String id, @NonNull String def) {
        return translate(locale, id, def);
    }

    public String translate(@NonNull Locale locale, @NonNull String id, @NonNull String def) {
        return getNode(locale, id)
                .map(TranslationNode::getValue)
                .orElse(def);
    }

    public <T extends PancakeLanguage> String translate(@NonNull T node) {
        return translate(locale, node);
    }

    public <T extends PancakeLanguage> String translate(@NonNull Locale locale, @NonNull T node) {
        return getNode(locale, node.getNode().getId())
                .map(TranslationNode::getValue)
                .orElseGet(node::getValue);
    }

    public Optional<TranslationNode> getNode(@NonNull Locale locale, @NonNull String id) {
        return this.repository.getNode(locale, id);
    }

    public Optional<Language> getByLocale(@NonNull Locale locale) {
        return this.repository.getByLocale(locale);
    }

    public List<Language> getLanguages() {
        return this.repository.getLanguages();
    }

    public void register(@NonNull PancakePlugin plugin, @NonNull Class<? extends PancakeLanguage> languageClass) {
        initDataFolder();
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe unsafe = (Unsafe) theUnsafe.get(null);
            PancakeLanguage pack = ((PancakeLanguage) unsafe.allocateInstance(languageClass));

            Language language = pack.toStandardLanguage(datafolder);
            File file = language.getFile();
            List<TranslationNode> nodes = new ArrayList<>();

            // Load existing nodes from file
            loader.load(file)
                    .ifPresent(l -> {
                        nodes.addAll(l.getNodes());
                        if (!file.delete())
                            logger.warn("Cannot update %s language file!", l.getLocale().getDisplayLanguage(Locale.ROOT));
                    });
            // Add missing nodes from PancakeLanguage
            pack.getNodes()
                    .stream()
                    .filter(translationNode -> nodes.stream().noneMatch(translationNode1 -> translationNode1.getId().equals(translationNode.getId())))
                    .forEach(nodes::add);

            language.getNodes().clear();
            language.getNodes().addAll(nodes);

            loader.save(language);

            List<String> lines = new ArrayList<>(Arrays.asList("# " + plugin.getName(),
                    "#",
                    "# Developed by Roch Blondiaux",
                    "# www.roch-blondiaux.com",
                    "#",
                    "# Default " + language.getNodes() + " translation",
                    "# If you want to edit the translations copy this file and rename it",
                    "# The file name format is 'language_COUNTRY.lang'",
                    "#",
                    "# If you're translating this file, remember to NOT translate placeholders NOR keys!"));
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
                String line;
                while ((line = br.readLine()) != null)
                    lines.add(line);
            }

            PrintWriter pw = new PrintWriter(new FileWriter(file));
            lines.forEach(pw::println);
            pw.close();

            loadSilently();
        } catch (NoSuchFieldException | IllegalAccessException | InstantiationException | IOException e) {
            logger.error("An error occurred while loading default language:");
            e.printStackTrace();
        }
    }

    private void initDataFolder() {
        if (!datafolder.exists()) {
            if (!datafolder.mkdir())
                logger.error("Cannot create %s! Please check file permissions.", datafolder.getPath());
        } else if (!datafolder.isDirectory()) {
            if (!datafolder.delete())
                logger.error("%s is not a folder!", datafolder.getPath());
            else if (!datafolder.mkdir())
                logger.error("Cannot create %s! Please check file permissions.", datafolder.getPath());
        }
    }

    private void loadFromDataFolder() {
        try {
            Files.walk(datafolder.toPath())
                    .map(Path::toFile)
                    .filter(File::exists)
                    .filter(file -> file.getName().contains(".yml"))
                    .filter(file -> file.getName().contains("_"))
                    .filter(File::isFile)
                    .map(file -> loader.load(file))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(this.repository::add);
        } catch (IOException e) {
            logger.error("An error occurred while loading translations files!");
        }
    }

    @Override
    public List<PancakeDependency> getDependencies() {
        return Collections.singletonList(PancakeDependency.SNAKEYAML);
    }
}
