package com.nesty.pancakelibrary.localization.model.loader;

import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.localization.model.language.Language;
import com.nesty.pancakelibrary.localization.model.language.TranslationNode;
import com.nesty.pancakelibrary.localization.model.validator.LocaleValidator;
import com.nesty.pancakelibrary.localization.model.validator.PancakeLocaleValidator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class YmlLanguageLoader implements LanguageLoader {

    private final Yaml adapter;
    private final PancakeLogger logger;
    private final LocaleValidator localeValidator;

    public YmlLanguageLoader() {
        this.logger = PancakeLogger.get();
        this.localeValidator = new PancakeLocaleValidator();

        DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
        options.setAllowUnicode(true);
        this.adapter = new Yaml(options);
    }

    @Override
    public Optional<Language> load(@NonNull File file) {
        Locale locale = parseLocale(file);
        if (Objects.isNull(locale) || !localeValidator.isValid(locale)) {
            logger.error("Cannot load %s translation file because it's named wrong.", file.getName());
            return Optional.empty();
        }
        try (FileInputStream input = new FileInputStream(file)) {
            Map<String, String> rawData = adapter.load(input);
            return Optional.of(new Language(locale,
                    rawData.entrySet()
                            .stream()
                            .map(e -> new TranslationNode(reverseKeyFormat(e.getKey()), e.getValue()))
                            .collect(Collectors.toList()),
                    file));
        } catch (IOException e) {
            logger.error("An error occurred while reading %s translation file!", file.getName());
            return Optional.empty();
        }
    }

    @Override
    public void save(@NonNull Language language) {
        Map<String, String> translations = language.getNodes()
                .stream()
                .collect(Collectors.toMap(node -> formatKey(node.getId()), TranslationNode::getValue));
        try {
            if (!language.getFile().exists())
                language.getFile().createNewFile();
        } catch (IOException ignored) {
        }

        try {
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(language.getFile()), StandardCharsets.UTF_8);
            adapter.dump(translations, writer);
            writer.close();
        } catch (FileNotFoundException e) {
            logger.error("Cannot save %s translation file, because %s is not accessible!", language.getLocale().getDisplayLanguage(Locale.ROOT), language.getFile().getName());
        } catch (IOException e) {
            logger.error("An error occurred while saving %s translation file!", language.getLocale().getDisplayLanguage(Locale.ROOT));
        }
    }

    private String formatKey(@NonNull String key) {
        return key.replace("_", "-").toLowerCase();
    }

    private String reverseKeyFormat(@NonNull String key) {
        return key.replace("-", "_").toUpperCase();
    }

    private Locale parseLocale(@NonNull File file) {
        if (file.getName().length() == 0)
            return null;
        String locale = file.getName().substring(0, file.getName().lastIndexOf('.'));
        String[] parts = locale.split("_");
        switch (parts.length) {
            case 3:
                return new Locale(parts[0], parts[1], parts[2]);
            case 2:
                return new Locale(parts[0], parts[1]);
            case 1:
                return new Locale(parts[0]);
            default: {
                logger.warn("Invalid locale code! (%s)", locale);
                return null;
            }
        }
    }
}
