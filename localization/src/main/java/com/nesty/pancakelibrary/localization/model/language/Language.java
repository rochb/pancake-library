package com.nesty.pancakelibrary.localization.model.language;

import lombok.Data;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class Language {

    private final Locale locale;
    private final List<TranslationNode> nodes;
    private final File file;

}
