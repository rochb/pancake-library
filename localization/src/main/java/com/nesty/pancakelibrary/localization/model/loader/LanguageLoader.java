package com.nesty.pancakelibrary.localization.model.loader;

import com.nesty.pancakelibrary.localization.model.language.Language;
import lombok.NonNull;

import java.io.File;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface LanguageLoader {


    Optional<Language> load(@NonNull File file);

    void save(@NonNull Language language);

}
