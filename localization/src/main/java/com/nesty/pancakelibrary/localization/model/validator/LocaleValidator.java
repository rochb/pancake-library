package com.nesty.pancakelibrary.localization.model.validator;

import lombok.NonNull;

import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface LocaleValidator {

    boolean isValid(@NonNull Locale locale);

}
