package com.nesty.pancakelibrary.localization.model.validator;

import lombok.NonNull;

import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PancakeLocaleValidator implements LocaleValidator {

    @Override
    public boolean isValid(@NonNull Locale locale) {
        return locale.getISO3Language() != null
                && locale.getISO3Country() != null;
    }
}
