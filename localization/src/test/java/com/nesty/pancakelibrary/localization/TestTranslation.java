package com.nesty.pancakelibrary.localization;

import com.nesty.pancakelibrary.localization.model.language.PancakeLanguage;
import com.nesty.pancakelibrary.localization.model.language.TranslationNode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@RequiredArgsConstructor
public enum TestTranslation implements PancakeLanguage {
    VALUE_1("This is the translation 1"),
    VALUE_2("This is the translation 2"),
    VALUE_3("This is the translation 3"),
    VALUE_4("This is the translation 4"),
    VALUE_5("This is the translation 5");

    private final String value;

    @Override
    public TranslationNode getNode() {
        return new TranslationNode(name(), value);
    }

    @Override
    public Locale getLocale() {
        return new Locale("en", "US");
    }

    @Override
    public List<TranslationNode> getNodes() {
        return Arrays.stream(values())
                .map(TestTranslation::getNode)
                .collect(Collectors.toList());
    }
}
