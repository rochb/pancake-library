package com.nesty.pancakelibrary.localization.module;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.localization.LocalizationModule;
import com.nesty.pancakelibrary.localization.TestTranslation;
import com.nesty.pancakelibrary.localization.utils.TestUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LocalizationModuleTest {

    private static PancakePlugin plugin;
    private static LocalizationModule module;
    private static File file;
    private static File dataFolder;

    @BeforeAll
    static void init() {
        plugin = new PancakePlugin() {
            @Override
            public String getName() {
                return "Localization test plugin";
            }

            @Override
            public File getDataFolder() {
                return null;
            }
        };
        dataFolder = new File("src/test/resources/locales");
        module = new LocalizationModule(dataFolder, new Locale("en", "US"));
        file = new File(dataFolder, "en_US.yml");
    }

    @Order(1)
    @Test
    void registerPancakeLanguage() {
        module.register(plugin, TestTranslation.class);
        assert module.getLanguages().size() == 1;
        assert file.exists();
    }

    @Order(2)
    @Test
    void testTranslations() {
        for (TestTranslation value : TestTranslation.values())
            assert Objects.equals(value.getValue(), module.translate(value));
    }

    @Order(3)
    @Test
    void testTranslationReplacement() {
        String newValue = "aaaaaaaaaaaa";
        TestUtils.setValue(file, "value-1", newValue);
        module.load(plugin);
        assert module.translate(TestTranslation.VALUE_1).equals(newValue);
    }

    @Order(4)
    @Test
    void testUnknownLocale() {
        assert module.translate(new Locale("en", "US"), TestTranslation.VALUE_1).equals("aaaaaaaaaaaa");
        assert module.translate(new Locale("fr", "FR"), TestTranslation.VALUE_1).equals(TestTranslation.VALUE_1.getValue());
    }

    @Order(5)
    @Test
    void testTranslationDefinition() {
        assert module.translate("VALUE_1", "a").equals("aaaaaaaaaaaa");
        assert module.translate(new Locale("en", "US"), "VALUE_1", "a").equals("aaaaaaaaaaaa");
        assert module.translate(new Locale("fr", "FR"), "VALUE_1", "a").equals("a");
    }

    @AfterAll
    static void unload() {
        file.deleteOnExit();
        dataFolder.deleteOnExit();
    }
}