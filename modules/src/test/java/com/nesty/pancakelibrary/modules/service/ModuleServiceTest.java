package com.nesty.pancakelibrary.modules.service;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.dependencies.DependencyService;
import com.nesty.pancakelibrary.modules.TestErrorModule;
import com.nesty.pancakelibrary.modules.TestModule;
import com.nesty.pancakelibrary.modules.exceptions.ModuleAlreadyLoadedException;
import com.nesty.pancakelibrary.modules.exceptions.ModuleAlreadyRegisteredException;
import com.nesty.pancakelibrary.modules.exceptions.ModuleNotLoadedException;
import com.nesty.pancakelibrary.modules.exceptions.UnknownModuleException;
import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import org.junit.jupiter.api.*;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ModuleServiceTest {

    private static ModuleService service;

    @BeforeAll
    static void init() {
        service = new ModuleService(new PancakePlugin() {
            @Override
            public String getName() {
                return "Module Test Plugin";
            }

            @Override
            public File getDataFolder() {
                return null;
            }
        }, new DependencyService(new File("src/test/resources").toPath()));
        service.register(new TestModule("test"));
    }

    @Test
    @Order(1)
    void load() {
        service.load();
        assert service.getModules().size() == 1;
        assert service.getModules()
                .stream()
                .allMatch(PancakeModule::isEnabled);
    }

    @Test
    @Order(2)
    void reLoadError() {
        assertThrows(ModuleAlreadyLoadedException.class, () -> service.load());
    }

    @Test
    @Order(3)
    void registerWithSameName() {
        assertThrows(ModuleAlreadyRegisteredException.class, () -> service.register(new TestModule("test")));
    }

    @Test
    @Order(4)
    void unload() {
        service.unload();
        assert service.getModules()
                .stream()
                .noneMatch(PancakeModule::isEnabled);
    }

    @Test
    @Order(5)
    void loadError() {
        service.register(new TestErrorModule("error"));
        assert service.getModules()
                .stream()
                .noneMatch(PancakeModule::isEnabled);
    }

    @Test
    @Order(6)
    void unloadError() {
        service.unload();
        assert service.getModules()
                .stream()
                .noneMatch(PancakeModule::isEnabled);
    }

    @Test
    @Order(7)
    void unloadUnloadedModule() {
        assertThrows(ModuleNotLoadedException.class, () -> service.unregister(new TestModule("test")));
    }

    @Test
    @Order(8)
    void unloadUnknownModule() {
        assertThrows(UnknownModuleException.class, () -> service.unregister(new TestModule("ezezeze")));

    }
}