package com.nesty.pancakelibrary.modules;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.modules.model.module.JavaModule;
import lombok.NonNull;

import java.util.ArrayList;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TestErrorModule extends JavaModule {

    public TestErrorModule(String name) {
        super(name);
    }

    @Override
    public void load(@NonNull PancakePlugin plugin) {
        throw new NullPointerException("This is a stupid error");
    }

    @Override
    public void unload() {
        throw new NullPointerException("This is a stupid error");
    }
}
