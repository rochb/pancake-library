package com.nesty.pancakelibrary.modules;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.modules.model.module.JavaModule;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class TestModule extends JavaModule {

    public TestModule(String name) {
        super(name);
    }

    @Override
    public void load(@NonNull PancakePlugin plugin) {
        logger.info("Loading %s module", getName());

        logger.success("%s module successfully loaded!", getName());
    }

    @Override
    public void unload() {
        logger.info("Unloading %s module", getName());

        logger.success("%s module successfully unloaded!", getName());
    }
}
