package com.nesty.pancakelibrary.modules.repository;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.modules.model.module.JavaModule;
import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.NonNull;
import org.junit.jupiter.api.*;

import java.io.File;
import java.util.Optional;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ModuleRepositoryTest {

    private static ModuleRepository repository;
    private static JavaModule module;

    @BeforeAll
    static void init() {
        repository = new ModuleRepository();
        module = new JavaModule("test") {
            @Override
            public void load(@NonNull PancakePlugin plugin) {
            }

            @Override
            public void unload() {
            }
        };
    }

    @Test
    @Order(1)
    void add() {
        repository.add(module);
        assert repository.getModules().size() == 1;
        assert repository.getModules().get(0).equals(module);
    }

    @Test
    @Order(2)
    void getByName() {
        Optional<PancakeModule> m = repository.getByName(module.getName());
        assert m.isPresent();
        assert m.get().equals(module);
    }

    @Order(3)
    @Test
    void getModules() {
        assert repository.getModules().size() == 1;
    }

    @Test
    @Order(4)
    void remove() {
        repository.remove(module);
        assert repository.getModules().size() == 0;
    }
}