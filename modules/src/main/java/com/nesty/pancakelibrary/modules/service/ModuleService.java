package com.nesty.pancakelibrary.modules.service;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.commons.utils.TimeRecorder;
import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.dependencies.DependencyService;
import com.nesty.pancakelibrary.modules.CommonModules;
import com.nesty.pancakelibrary.modules.exceptions.ModuleAlreadyRegisteredException;
import com.nesty.pancakelibrary.modules.exceptions.UnknownModuleException;
import com.nesty.pancakelibrary.modules.model.loader.JavaModuleLoader;
import com.nesty.pancakelibrary.modules.model.loader.ModuleLoader;
import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import com.nesty.pancakelibrary.modules.repository.IModuleRepository;
import com.nesty.pancakelibrary.modules.repository.ModuleRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class ModuleService {

    private final IModuleRepository repository;
    private final PancakeLogger logger = PancakeLogger.get();
    private final ModuleLoader loader;

    public ModuleService(@NonNull PancakePlugin plugin, @NonNull DependencyService service) {
        this.loader = new JavaModuleLoader(plugin, service);
        this.repository = new ModuleRepository();
    }

    public void register(@NonNull PancakeModule module) {
        if (getByName(module.getName()).isPresent())
            throw new ModuleAlreadyRegisteredException(String.format("%s module is already registered!", module.getName()));
        this.repository.add(module);
    }

    public void unregister(@NonNull PancakeModule module) {
        if (!getByName(module.getName()).isPresent())
            throw new UnknownModuleException(String.format("Cannot unregister %s module because it doesn't exists!", module.getName()));
        this.repository.remove(module);
        loader.unload(module);
    }

    public void load() {
        logger.info("Loading modules...");
        TimeRecorder recorder = new TimeRecorder();
        Arrays.stream(CommonModules.values())
                .map(CommonModules::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(pancakeModule -> !getModules().contains(pancakeModule))
                .forEach(getModules()::add);
        getModules().forEach(loader::load);
        logger.success("Modules successfully loaded! (Took %d ms)", recorder.getDurationFromStart());
    }

    public void reload() {
        logger.info("Reloading modules...");
        TimeRecorder recorder = new TimeRecorder();
        getModules()
                .stream()
                .filter(PancakeModule::isEnabled)
                .forEach(loader::unload);
        getModules().forEach(loader::load);
        logger.success("Modules successfully reloaded! (Took %d ms)", recorder.getDurationFromStart());
    }

    public void unload() {
        logger.info("Unloading modules...");
        TimeRecorder recorder = new TimeRecorder();
        getModules()
                .stream()
                .filter(PancakeModule::isEnabled)
                .forEach(loader::unload);
        logger.success("Modules successfully unloaded! (Took %d ms)", recorder.getDurationFromStart());
    }

    public Optional<PancakeModule> getByName(@NonNull String name) {
        return this.repository.getByName(name);
    }

    public List<PancakeModule> getModules() {
        return this.repository.getModules();
    }
}
