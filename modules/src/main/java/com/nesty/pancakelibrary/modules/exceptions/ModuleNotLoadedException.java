package com.nesty.pancakelibrary.modules.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ModuleNotLoadedException extends IllegalStateException {

    public ModuleNotLoadedException(String s) {
        super(s);
    }
}
