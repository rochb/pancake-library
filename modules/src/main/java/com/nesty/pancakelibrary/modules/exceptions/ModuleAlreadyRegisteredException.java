package com.nesty.pancakelibrary.modules.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ModuleAlreadyRegisteredException extends RuntimeException {

    public ModuleAlreadyRegisteredException(String message) {
        super(message);
    }
}
