package com.nesty.pancakelibrary.modules.model.module;

import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
@EqualsAndHashCode
@Setter
public abstract class JavaModule implements PancakeModule {

    private final String name;
    protected final PancakeLogger logger = PancakeLogger.get();
    private boolean enabled = false;

}
