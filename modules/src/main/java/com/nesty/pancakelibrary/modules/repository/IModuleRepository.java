package com.nesty.pancakelibrary.modules.repository;

import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface IModuleRepository {

    void add(@NonNull PancakeModule module);

    void remove(@NonNull PancakeModule module);

    Optional<PancakeModule> getByName(@NonNull String name);

    List<PancakeModule> getModules();

}
