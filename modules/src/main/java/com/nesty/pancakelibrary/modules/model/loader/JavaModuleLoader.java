package com.nesty.pancakelibrary.modules.model.loader;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import com.nesty.pancakelibrary.commons.utils.TimeRecorder;
import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.dependencies.DependencyService;
import com.nesty.pancakelibrary.modules.exceptions.ModuleAlreadyLoadedException;
import com.nesty.pancakelibrary.modules.exceptions.ModuleNotLoadedException;
import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.NonNull;

import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class JavaModuleLoader implements ModuleLoader {

    private final PancakePlugin plugin;
    private final PancakeLogger logger;
    private final DependencyService service;

    public JavaModuleLoader(@NonNull PancakePlugin plugin, @NonNull DependencyService service) {
        this.plugin = plugin;
        this.logger = PancakeLogger.get();
        this.service = service;
    }

    @Override
    public void load(@NonNull PancakeModule module) {
        logger.info("Loading %s module...", module.getName());
        TimeRecorder recorder = new TimeRecorder();

        if (module.isEnabled())
            throw new ModuleAlreadyLoadedException(String.format("Module %s is already loaded!", module.getName()));
        try {
            PancakeDependency[] unloadedDependencies = module.getDependencies()
                    .stream()
                    .filter(pancakeDependency -> !service.isLoaded(pancakeDependency))
                    .collect(Collectors.toList())
                    .toArray(new PancakeDependency[]{});
            if (unloadedDependencies.length > 0)
                this.service.loadDependencies(unloadedDependencies);

            module.load(plugin);
            module.setEnabled(true);
            logger.success("%s module loaded successfully! (Took %d ms)", module.getName(), recorder.getDurationFromStart());
        } catch (Exception e) {
            module.setEnabled(false);
            logger.error("An error occurred while loading %s module!", module.getName());
            e.printStackTrace();
        }
    }

    @Override
    public void unload(@NonNull PancakeModule module) {
        logger.info("Unloading %s module...", module.getName());
        if (!module.isEnabled())
            throw new ModuleNotLoadedException(String.format("Cannot unload %s module. Because it isn't loaded yet!", module.getName()));
        TimeRecorder recorder = new TimeRecorder();
        try {
            module.load(plugin);
            module.setEnabled(false);
            logger.success("%s module unloaded successfully! (Took %d ms)", module.getName(), recorder.getDurationFromStart());
        } catch (Exception e) {
            module.setEnabled(true);
            logger.error("An error occurred while unloading %s module!", module.getName());
            e.printStackTrace();
        }
    }
}
