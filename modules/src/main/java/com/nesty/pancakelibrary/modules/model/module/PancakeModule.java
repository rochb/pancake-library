package com.nesty.pancakelibrary.modules.model.module;

import com.nesty.pancakelibrary.commons.PancakePlugin;
import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PancakeModule {

    /**
     * Get module full name.
     *
     * @return module's name
     */
    String getName();

    /**
     * Method called on plugin enable.
     *
     * @param plugin module's parent.
     */
    void load(@NonNull PancakePlugin plugin);

    /**
     * Method called on plugin disable.
     */
    void unload();

    /**
     * Checks if module is enabled.
     *
     * @return true if it's the case, otherwise, false.
     */
    boolean isEnabled();

    /**
     * Set module as enabled or not.
     *
     * @param enabled true if module loaded successfully otherwise false.
     */
    void setEnabled(boolean enabled);

    /**
     * Get module's dependencies.
     *
     * @return list of {@link PancakeDependency} required to load
     * this module.
     */
    default List<PancakeDependency> getDependencies() {
        return new ArrayList<>();
    }
}
