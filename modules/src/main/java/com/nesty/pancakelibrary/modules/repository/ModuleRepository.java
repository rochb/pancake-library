package com.nesty.pancakelibrary.modules.repository;

import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class ModuleRepository implements IModuleRepository {

    private final List<PancakeModule> modules = new ArrayList<>();

    @Override
    public void add(@NonNull PancakeModule module) {
        this.modules.add(module);
    }

    @Override
    public void remove(@NonNull PancakeModule module) {
        this.modules.remove(module);
    }

    @Override
    public Optional<PancakeModule> getByName(@NonNull String name) {
        return modules.stream()
                .filter(module -> module.getName().equals(name))
                .findFirst();
    }

}
