package com.nesty.pancakelibrary.modules;

import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public enum CommonModules {
    LOCALIZATION("localization", "com.nesty.pancakelibrary.localization.LocalizationModule"),
    CONFIGURATION("configuration", "com.nesty.pancakelibrary.configuration.ConfigurationModule"),
    DEPENDENCIES("dependencies", "com.nesty.pancakelibrary.dependencies.DependenciesModule"),
    ;

    private final String name;
    private final String classPath;

    public Optional<PancakeModule> get() {
        try {
            Class<? extends PancakeModule> clazz = (Class<? extends PancakeModule>) Class.forName(classPath);
            return Optional.ofNullable(clazz.newInstance());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            return Optional.empty();
        }
    }

    public boolean isPresent() {
        try {
            Class.forName(classPath);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
