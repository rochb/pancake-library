package com.nesty.pancakelibrary.modules.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class UnknownModuleException extends NullPointerException {

    public UnknownModuleException(String s) {
        super(s);
    }
}
