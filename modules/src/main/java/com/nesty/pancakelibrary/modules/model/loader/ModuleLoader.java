package com.nesty.pancakelibrary.modules.model.loader;

import com.nesty.pancakelibrary.modules.model.module.PancakeModule;
import lombok.NonNull;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface ModuleLoader {

    void load(@NonNull PancakeModule module);

    void unload(@NonNull PancakeModule module);
}
