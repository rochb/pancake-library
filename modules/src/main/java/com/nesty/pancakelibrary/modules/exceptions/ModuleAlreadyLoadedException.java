package com.nesty.pancakelibrary.modules.exceptions;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ModuleAlreadyLoadedException extends IllegalStateException {

    public ModuleAlreadyLoadedException(String s) {
        super(s);
    }
}
