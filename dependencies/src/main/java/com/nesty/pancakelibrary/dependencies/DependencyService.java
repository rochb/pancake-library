package com.nesty.pancakelibrary.dependencies;

import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import lombok.NonNull;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DependencyService {

    private final DependencyManager manager;

    public DependencyService(@NonNull Path dataFolder) {
        manager = new DependencyManager(dataFolder);
    }

    public void loadDependencies(@NonNull Dependency... dependencies) {
        this.manager.loadDependencies(new HashSet<>(Arrays.asList(dependencies)));
    }

    public void loadDependencies(@NonNull PancakeDependency... dependencies) {
        this.manager.loadDependencies(Arrays.stream(dependencies)
                .map(dependency -> Arrays.stream(Dependency.values())
                        .filter(dependency1 -> dependency1.getPancakeDependency().equals(dependency))
                        .findFirst())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet()));
    }

    public boolean isLoaded(@NonNull PancakeDependency dependency) {
        return this.manager.getLoaded()
                .keySet()
                .stream()
                .anyMatch(dependency1 -> dependency1.getPancakeDependency().equals(dependency));
    }

    public boolean isLoaded(@NonNull Dependency dependency) {
        return this.manager.getLoaded().containsKey(dependency);
    }
}
