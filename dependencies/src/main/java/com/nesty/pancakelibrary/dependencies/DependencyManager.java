package com.nesty.pancakelibrary.dependencies;

import com.google.common.collect.ImmutableSet;
import com.nesty.pancakelibrary.commons.dependency.Relocation;
import com.nesty.pancakelibrary.commons.utils.logging.PancakeLogger;
import com.nesty.pancakelibrary.dependencies.classloader.IsolatedClassLoader;
import com.nesty.pancakelibrary.dependencies.exceptions.DependencyDownloadException;
import com.nesty.pancakelibrary.dependencies.reflection.ReflectionClassPathAppender;
import com.nesty.pancakelibrary.dependencies.relocation.RelocationHandler;
import lombok.Getter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DependencyManager {

    /**
     * A registry containing plugin specific behaviour for dependencies.
     */
    private final DependencyRegistry registry;

    /**
     * The path where library jars are cached.
     */
    private final Path cacheDirectory;

    /**
     * A map of dependencies which have already been loaded.
     */
    @Getter
    private final EnumMap<Dependency, Path> loaded = new EnumMap<>(Dependency.class);

    /**
     * A map of isolated classloaders which have been created.
     */
    private final Map<ImmutableSet<Dependency>, IsolatedClassLoader> loaders = new HashMap<>();

    /**
     * Cached relocation handler instance.
     */
    private RelocationHandler relocationHandler = null;

    private static final PancakeLogger logger = PancakeLogger.get();

    public DependencyManager(Path datafolder) {
        this.registry = new DependencyRegistry();
        this.cacheDirectory = setupCacheDirectory(datafolder);
    }

    private synchronized RelocationHandler getRelocationHandler() {
        if (this.relocationHandler == null)
            this.relocationHandler = new RelocationHandler(this);
        return this.relocationHandler;
    }

    public IsolatedClassLoader obtainClassLoaderWith(Set<Dependency> dependencies) {
        ImmutableSet<Dependency> set = ImmutableSet.copyOf(dependencies);

        for (Dependency dependency : dependencies) {
            if (!this.loaded.containsKey(dependency)) {
                throw new IllegalStateException("Dependency " + dependency + " is not loaded.");
            }
        }

        synchronized (this.loaders) {
            IsolatedClassLoader classLoader = this.loaders.get(set);
            if (classLoader != null)
                return classLoader;

            URL[] urls = set.stream()
                    .map(this.loaded::get)
                    .map(file -> {
                        try {
                            return file.toUri().toURL();
                        } catch (MalformedURLException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .toArray(URL[]::new);

            classLoader = new IsolatedClassLoader(urls);
            this.loaders.put(set, classLoader);
            return classLoader;
        }
    }

    public void loadDependencies(Set<Dependency> dependencies) {
        CountDownLatch latch = new CountDownLatch(dependencies.size());

        for (Dependency dependency : dependencies) {
            try {
                loadDependency(dependency);
            } catch (Throwable e) {
                logger.error("Unable to load dependency " + dependency.name() + ".", e);
            } finally {
                latch.countDown();
            }
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void loadDependency(Dependency dependency) throws Exception {
        if (this.loaded.containsKey(dependency))
            return;

        Path file = remapDependency(dependency, downloadDependency(dependency));

        this.loaded.put(dependency, file);

        if (this.registry.shouldAutoLoad(dependency))
            new ReflectionClassPathAppender().addJarToClasspath(file);
    }

    private Path downloadDependency(Dependency dependency) throws DependencyDownloadException {
        Path file = this.cacheDirectory.resolve(dependency.getFileName(null));

        // if the file already exists, don't attempt to re-download it.
        if (Files.exists(file))
            return file;
        logger.info("Downloading %s...", dependency.name());
        DependencyDownloadException lastError = null;

        // attempt to download the dependency from each repo in order.
        for (DependencyRepository repo : DependencyRepository.values()) {
            try {
                repo.download(dependency, file);
                return file;
            } catch (DependencyDownloadException e) {
                lastError = e;
            }
        }

        throw Objects.requireNonNull(lastError);
    }

    private Path remapDependency(Dependency dependency, Path normalFile) throws Exception {
        List<Relocation> rules = new ArrayList<>(dependency.getRelocations());

        if (rules.isEmpty())
            return normalFile;

        Path remappedFile = this.cacheDirectory.resolve(dependency.getFileName("remapped"));

        // if the remapped source exists already, just use that.
        if (Files.exists(remappedFile))
            return remappedFile;

        getRelocationHandler().remap(normalFile, remappedFile, rules);
        return remappedFile;
    }

    private static Path setupCacheDirectory(Path dataFolder) {
        Path cacheDirectory = dataFolder.resolve("libs");
        try {
            Files.createDirectories(cacheDirectory);
        } catch (IOException e) {
            throw new RuntimeException("Unable to create libs directory", e);
        }

        Path oldCacheDirectory = dataFolder.resolve("lib");
        if (Files.exists(oldCacheDirectory)) {
            try {
                Files.delete(oldCacheDirectory);
            } catch (IOException e) {
                logger.warn("Unable to delete lib directory", e);
            }
        }

        return cacheDirectory;
    }

}