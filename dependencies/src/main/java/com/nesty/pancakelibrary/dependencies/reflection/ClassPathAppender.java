package com.nesty.pancakelibrary.dependencies.reflection;

import java.nio.file.Path;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 * <p>
 * Interface which allows access to add URLs to the plugin classpath at runtime.
 */
public interface ClassPathAppender extends AutoCloseable {

    void addJarToClasspath(Path file);

    @Override
    default void close() {

    }
}