package com.nesty.pancakelibrary.dependencies;

import com.google.common.collect.ImmutableList;
import com.nesty.pancakelibrary.commons.dependency.PancakeDependency;
import com.nesty.pancakelibrary.commons.dependency.Relocation;
import lombok.Getter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum Dependency {
    ASM(PancakeDependency.ASM),
    ASM_COMMONS(PancakeDependency.ASM_COMMONS),
    JAR_RELOCATOR(PancakeDependency.JAR_RELOCATOR),
    MARIADB_DRIVER(PancakeDependency.MARIADB_DRIVER),
    MYSQL_DRIVER(PancakeDependency.MYSQL_DRIVER),
    POSTGRESQL_DRIVER(PancakeDependency.POSTGRESQL_DRIVER),
    H2_DRIVER(PancakeDependency.H2_DRIVER),
    SQLITE_DRIVER(PancakeDependency.SQLITE_DRIVER),
    HIKARI(PancakeDependency.HIKARI),
    JEDIS(PancakeDependency.JEDIS),
    RABBITMQ(PancakeDependency.RABBITMQ),
    COMMONS_POOL_2(PancakeDependency.COMMONS_POOL_2),
    SNAKEYAML(PancakeDependency.SNAKEYAML);

    @Getter
    private final PancakeDependency pancakeDependency;
    private final String mavenRepoPath;
    private final String version;
    private final byte[] checksum;
    private final List<Relocation> relocations;

    private static final String MAVEN_FORMAT = "%s/%s/%s/%s-%s.jar";

    Dependency(PancakeDependency dependency) {
        this.pancakeDependency = dependency;
        this.mavenRepoPath = String.format(MAVEN_FORMAT,
                rewriteEscaping(dependency.getGroupId()).replace(".", "/"),
                rewriteEscaping(dependency.getArtifactId()),
                dependency.getVersion(),
                rewriteEscaping(dependency.getArtifactId()),
                dependency.getVersion()
        );
        this.version = dependency.getVersion();
        this.checksum = Base64.getDecoder().decode(dependency.getChecksum());
        this.relocations = ImmutableList.copyOf(dependency.getRelocations());
    }

    private static String rewriteEscaping(String s) {
        return s.replace("{}", ".");
    }

    public String getFileName(String classifier) {
        String name = name().toLowerCase(Locale.ROOT).replace('_', '-');
        String extra = classifier == null || classifier.isEmpty()
                ? ""
                : "-" + classifier;

        return name + "-" + this.version + extra + ".jar";
    }

    String getMavenRepoPath() {
        return this.mavenRepoPath;
    }

    public byte[] getChecksum() {
        return this.checksum;
    }

    public boolean checksumMatches(byte[] hash) {
        return Arrays.equals(this.checksum, hash);
    }

    public List<Relocation> getRelocations() {
        return this.relocations;
    }

    /**
     * Creates a {@link MessageDigest} suitable for computing the checksums
     * of dependencies.
     *
     * @return the digest
     */
    public static MessageDigest createDigest() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}