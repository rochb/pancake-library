package com.nesty.pancakelibrary.bungeecord.tasks;

import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.plugin.Plugin;
import net.nesty.pancakelibrary.tasks.model.ExecutionStrategy;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class BungeeRepeatingExecution implements ExecutionStrategy {

    private final Plugin plugin;
    private final int interval;

    @Override
    public int execute(Runnable runnable) {
        return plugin.getProxy().getScheduler().schedule(
                plugin,
                runnable,
                0L,
                interval,
                TimeUnit.SECONDS
        ).getId();
    }
}
