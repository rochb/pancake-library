package com.nesty.pancakelibrary.bungeecord.tasks;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.nesty.pancakelibrary.tasks.model.TaskService;
import net.nesty.pancakelibrary.tasks.model.TimerFactory;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class BungeecordTaskService implements TaskService {

    private final BungeeTaskFactory factory;

    public BungeecordTaskService(Plugin plugin) {
        this.factory = new BungeeTaskFactory(ProxyServer.getInstance().getScheduler(), plugin);
    }

    @Override
    public TimerFactory getTimerFactory() {
        return null;
    }
}
