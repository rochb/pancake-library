package com.nesty.pancakelibrary.bungeecord.tasks;

import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import net.nesty.pancakelibrary.tasks.model.AbstractRepeatingTask;
import net.nesty.pancakelibrary.tasks.model.Task;
import net.nesty.pancakelibrary.tasks.model.TaskFactory;
import net.nesty.pancakelibrary.tasks.model.async.Callback;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingRunnable;
import net.nesty.pancakelibrary.tasks.model.repeating.RepeatingTask;
import net.nesty.pancakelibrary.tasks.model.type.CompletableTask;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class BungeeTaskFactory implements TaskFactory {

    private final TaskScheduler taskScheduler;
    private final Plugin plugin;

    @Override
    public Task newSyncTask(Runnable runnable) {
        return failSync();
    }

    @Override
    public Task newSyncDelayedTask(Runnable runnable, int delayInSeconds) {
        return failSync();
    }

    @Override
    public Task newAsyncDelayedTask(Runnable runnable, int delayInSeconds) {
        Task task = () -> taskScheduler.schedule(plugin, runnable, delayInSeconds, TimeUnit.SECONDS);
        task.execute();
        return task;
    }

    @Override
    public <R> CompletableTask<R> newAsyncFunction(Supplier<R> task) {
        return new CompletableTask<R>() {
            @Override
            public void completeAsynchronously(Callback<R> result) {
                taskScheduler.runAsync(plugin, () -> result.call(task.get()));
            }

            @Override
            public void completeSynchronously(Callback<R> result) {
                failSync();
            }
        };
    }

    @Override
    public Task newAsyncTask(Runnable runnable) {
        Task task = () -> taskScheduler.runAsync(plugin, runnable);
        task.execute();
        return task;
    }

    @Override
    public RepeatingTask newRepeatingTask(Runnable runnable, int period) {
        AbstractRepeatingTask task = new AbstractRepeatingTask(
                new BungeeSchedulerTask(
                        new BungeeRepeatingExecution(plugin, period),
                        taskScheduler,
                        new RepeatingRunnable(runnable)
                )
        );
        task.start();
        return task;
    }

    private <T> T failSync() {
        throw new UnsupportedOperationException("BungeeCord does not support synchronous tasks!");
    }
}
