package com.nesty.pancakelibrary.commons.utils.logging;

import com.nesty.pancakelibrary.commons.utils.console.ConsoleSymbol;

import java.util.Objects;
import java.util.logging.Logger;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PancakeLogger {

    private static PancakeLogger instance;

    public void info(String message) {
        Logger.getLogger(ConsoleSymbol.INFO).info(message);
    }

    public void info(String message, Object... placeholders) {
        info(String.format(message, placeholders));
    }

    public void warn(String message) {
        Logger.getLogger(ConsoleSymbol.WARN).warning(message);
    }

    public void warn(String message, Object... placeholders) {
        warn(String.format(message, placeholders));
    }

    public void error(String message) {
        Logger.getLogger(ConsoleSymbol.ERROR).severe(message);
    }

    public void error(String message, Object... placeholders) {
        error(String.format(message, placeholders));
    }

    public void success(String message) {
        Logger.getLogger(ConsoleSymbol.SUCCESS).info(message);
    }

    public void success(String message, Object... placeholders) {
        success(String.format(message, placeholders));
    }

    public static PancakeLogger get() {
        if (Objects.isNull(instance))
            instance = new PancakeLogger();
        return instance;
    }
}
