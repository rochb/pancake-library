package com.nesty.pancakelibrary.commons.utils.console;

import lombok.experimental.UtilityClass;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@UtilityClass
public class ConsoleSymbol {

    public static final String SUCCESS = ConsoleColors.WHITE + "[" + ConsoleColors.GREEN_BOLD + "✓" + ConsoleColors.WHITE + "]" + ConsoleColors.RESET + " ";
    public static final String INFO = ConsoleColors.WHITE + "[" + ConsoleColors.BLUE_BOLD + "i" + ConsoleColors.WHITE + "]" + ConsoleColors.RESET + " ";
    public static final String WARN = ConsoleColors.WHITE + "[" + ConsoleColors.YELLOW_BOLD + "✕" + ConsoleColors.WHITE + "]" + ConsoleColors.RESET + " ";
    public static final String ERROR = ConsoleColors.WHITE + "[" + ConsoleColors.RED_BOLD + "✕" + ConsoleColors.WHITE + "]" + ConsoleColors.RESET + " ";

}
