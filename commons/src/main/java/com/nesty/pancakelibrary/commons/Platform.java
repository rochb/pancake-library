package com.nesty.pancakelibrary.commons;

import lombok.NonNull;

import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;

/**
 * Provides information about the platform LuckPerms is running on.
 */
public interface Platform {

    /**
     * Gets the type of platform LuckPerms is running on
     *
     * @return the type of platform LuckPerms is running on
     */
    Platform.@NonNull Type getType();

    /**
     * Gets the unique players which have connected to the server since it started.
     *
     * @return the unique connections
     */
    @NonNull
    Set<UUID> getUniqueConnections();

    /**
     * Gets a {@link Collection} of all known permission strings.
     *
     * @return a collection of the known permissions
     */
    @NonNull
    Collection<String> getKnownPermissions();

    /**
     * Gets the time when the plugin first started.
     *
     * @return the enable time
     */
    @NonNull
    Instant getStartTime();

    /**
     * Represents a type of platform which LuckPerms can run on.
     */
    enum Type {
        BUKKIT("Bukkit"),
        BUNGEECORD("BungeeCord"),
        SPONGE("Sponge"),
        NUKKIT("Nukkit"),
        VELOCITY("Velocity"),
        FABRIC("Fabric");

        private final String friendlyName;

        Type(String friendlyName) {
            this.friendlyName = friendlyName;
        }

        /**
         * Gets a readable name for the platform type.
         *
         * @return a readable name
         */
        public @NonNull
        String getFriendlyName() {
            return this.friendlyName;
        }
    }
}