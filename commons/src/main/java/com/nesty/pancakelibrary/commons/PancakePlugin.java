package com.nesty.pancakelibrary.commons;


import java.io.File;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PancakePlugin {

    String getName();

    File getDataFolder();
}
