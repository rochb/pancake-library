package com.nesty.pancakelibrary.commons.utils;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class TimeRecorder {

    private final long start;

    public TimeRecorder() {
        this.start = System.currentTimeMillis();
    }

    public long getDurationFromStart() {
        return System.currentTimeMillis() - start;
    }
}
