package com.nesty.pancakelibrary.commons.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@UtilityClass
public class ReflectionUtils {

    public static Optional<Class<?>> getGenericType(@NonNull Class<?> targetClass, int i) {
        Type[] types = targetClass.getGenericInterfaces();
        if (types.length <= i)
            return Optional.empty();
        if (!(types[i] instanceof ParameterizedType))
            return Optional.empty();
        return Optional.of((Class<?>) ((ParameterizedType) types[i]).getActualTypeArguments()[0]);
    }

    public static Optional<Class<?>> getGenericType(@NonNull Class<?> targetClass) {
        return getGenericType(targetClass, 0);
    }
}
