package com.nesty.pancakelibrary.commons;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface PancakeBootstrap {

    void loadIntegratedServices();

    void load();

    void unload();

    void reload();

    PancakePlugin getPlugin();
}
